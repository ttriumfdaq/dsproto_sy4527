#include "caenhv_fe_class.h"
#include "caenhvwrapper_cxx.h"
#include "midas.h"
#include <vector>
#include <string>
#include <string.h>
#include <sstream>
#include <map>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <sys/time.h>

void global_settings_update_helper(HNDLE hDB, HNDLE hkey, void* info) {
  if (info) {
     CaenHVFE* fe = (CaenHVFE*)info;
     fe->odb_global_settings_changed_callback();
  }
}

void slot_settings_update_helper(HNDLE hDB, HNDLE hkey, void* info) {
  if (info) {
     CaenHVFE* fe = (CaenHVFE*)info;
     fe->odb_slot_settings_changed_callback();
  }
}

CaenHVFE::CaenHVFE() {
   changes_pending = false;
   eqp_name = "";
   sys_type = SY1527; // Will be set later by user in frontend_init()
   hDB = 0;
   hBase = 0;
   hSettings = 0;
   hGlobalSettings = 0;
   hStatus = 0;
   sync_period_ms = 0;
}

CaenHVFE::~CaenHVFE() {}

void CaenHVFE::set_ignore_param(std::string param_name) {
   ignore_params.insert(param_name);
}

void CaenHVFE::set_log_param(std::string param_name, std::string bank_prefix) {
   while (bank_prefix.size() < 2) {
      bank_prefix += "X";
   }
   bank_prefix.resize(2);
   log_params[param_name] = bank_prefix;
}

std::string CaenHVFE::get_bank_name(std::string param_name, int slot) {
   if (log_params.find(param_name) == log_params.end() || slot < 0 || slot > 99) {
      return "ERRR";
   }
   char bn[5];
   sprintf(bn, "%s%02d", log_params[param_name].c_str(), slot);
   return bn;
}

void CaenHVFE::set_odb_alias(std::string param_name, std::string odb_name) {
   odb_name.resize(32);
   odb_aliases[param_name] = odb_name;
}

void CaenHVFE::set_odb_type(std::string param_name, INT midas_type) {
   // Currently just support TID_BOOL instead of TID_DWORD for Unsigned params
   odb_types[param_name] = midas_type;
}

void CaenHVFE::set_sync_period_ms(float ms) {
   sync_period_ms = ms;
}

INT CaenHVFE::sync() {
   if (crate.handle == -1) {
      // Not initialized yet.
      return SUCCESS;
   }

   // Only sync every so often.
   if (sync_period_ms > 0) {
      timeval now;
      gettimeofday(&now, 0);

      double dt_ms = (now.tv_sec - last_sync_time.tv_sec) * 1000;
      dt_ms += (now.tv_usec - last_sync_time.tv_usec) / 1000;

      if (dt_ms < sync_period_ms) {
         return SUCCESS;
      }

      last_sync_time = now;
   }

   // Yield to ensure changes_pending is up-to-date.
   cm_yield(0);

   if (changes_pending) {
      changes_pending = false;
      INT status = write_odb_changes_to_crate();

      if (status == SUCCESS) {
         wait_for_changes_to_be_reflected_by_crate();
      } else {
         cm_msg(MERROR, __FUNCTION__, "Failed to send new values to crate");
         return status;
      }
   }

   // Read current values from board.
   CAENHVRESULT read_status = crate.read_all_param_values();

   if (read_status != CAENHV_OK) {
      cm_msg(MERROR, __FUNCTION__, "Failed to read state of CAEN HV crate: %d", read_status);
   }

   INT write_status = write_state_to_odb();

   if (write_status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to update ODB.");
      return write_status;
   }

   return SUCCESS;
}

INT CaenHVFE::record_state(char* pevent, INT off) {
   INT sync_status = sync();

   // Build event
   INT event_size = 0;
   INT event_status = create_event(pevent, event_size);

   if (event_status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to create event.");
      return 0;
   }

   return event_size;
}

INT CaenHVFE::create_event(char* pevent, INT& event_size) {
   event_size = 0;
   INT status;

   if (log_params.size() == 0) {
      return SUCCESS;
   }

   bk_init32(pevent);

   for (auto modit = crate.modules.begin(); modit != crate.modules.end(); modit++) {
      CAENHV::MODULE mod = (*modit);

      std::map<CAENHV::ParamBase*, ParamOdbInfo> all_slot_params;
      all_slot_params.insert(odb_info_slot_settings[mod.slot].begin(), odb_info_slot_settings[mod.slot].end());
      all_slot_params.insert(odb_info_slot_status[mod.slot].begin(), odb_info_slot_status[mod.slot].end());

      for (auto it = all_slot_params.begin(); it != all_slot_params.end(); it++) {
         CAENHV::ParamBase* param = it->first;

         if (log_params.find(param->name) != log_params.end()) {
            std::string bankname = get_bank_name(param->name, mod.slot);

            switch (param->type) {
               case CAENHV::ParamType::Numeric:
                  status = add_to_event(pevent, (CAENHV::ParamFloatBase*)param, it->second, bankname);
                  if (status != SUCCESS) return status;
                  break;
               case CAENHV::ParamType::Binary:
               case CAENHV::ParamType::BoardStatus:
               case CAENHV::ParamType::ChanStatus:
               case CAENHV::ParamType::Enum:
               case CAENHV::ParamType::OnOff:
                  status = add_to_event(pevent, (CAENHV::ParamUnsignedBase*)param, it->second, bankname);
                  if (status != SUCCESS) return status;
                  break;
               default:
                  cm_msg(MERROR, __FUNCTION__, "Unhandled param type %d when creating event", param->type);
                  break;
            }
         }
      }
   }

   event_size = bk_size(pevent);
   return SUCCESS;
}

INT CaenHVFE::write_state_to_odb() {
   return sync_odb_crate_changes(true);
}

INT CaenHVFE::write_odb_changes_to_crate() {
   return sync_odb_crate_changes(false);
}

INT CaenHVFE::sync_odb_crate_changes(bool to_odb) {
   INT status;

   for (auto modit = crate.modules.begin(); modit != crate.modules.end(); modit++) {     
      if (changes_pending) {
         break;
      }
             
      CAENHV::MODULE mod = (*modit);

      // Always work on RW parameters
      std::map<CAENHV::ParamBase*, ParamOdbInfo> all_slot_params;
      all_slot_params.insert(odb_info_slot_settings[mod.slot].begin(), odb_info_slot_settings[mod.slot].end());

      if (to_odb) {
         // Also work on R parameters when updating ODB
         all_slot_params.insert(odb_info_slot_status[mod.slot].begin(), odb_info_slot_status[mod.slot].end());
      }

      for (auto it = all_slot_params.begin(); it != all_slot_params.end(); it++) {
         cm_yield(0);
         if (changes_pending) {
            break;
         }

         CAENHV::ParamBase* param = it->first;

         switch (param->type) {
            case CAENHV::ParamType::Numeric:
               status = sync_change((CAENHV::ParamFloatBase*)param, it->second, mod.slot, to_odb);
               if (status != SUCCESS) return status;
               break;
            case CAENHV::ParamType::Binary:
            case CAENHV::ParamType::BoardStatus:
            case CAENHV::ParamType::ChanStatus:
            case CAENHV::ParamType::Enum:
            case CAENHV::ParamType::OnOff:
               status = sync_change((CAENHV::ParamUnsignedBase*)param, it->second, mod.slot, to_odb);
               if (status != SUCCESS) return status;
               break;
            case CAENHV::ParamType::ChanName:
               status = sync_change((CAENHV::ParamChanName*)param, it->second, mod.slot, to_odb);
               if (status != SUCCESS) return status;
               break;
            default:
               cm_msg(MERROR, __FUNCTION__, "Unhandled param type %d when syncing data", param->type);
               break;
         }
      }
   }

   return SUCCESS;
}

INT CaenHVFE::wait_for_changes_to_be_reflected_by_crate() {
   INT status;

   timeval start, now;
   double timeout_secs = settings.check_timeout_secs;
   double delta_secs = 0;
   gettimeofday(&start, NULL);

   while (delta_secs < timeout_secs) {
      bool any_unchecked = false;
      gettimeofday(&now, NULL);
      delta_secs = (now.tv_sec - start.tv_sec) + 1e-6 * (now.tv_usec - start.tv_usec);
      crate.read_all_param_values();

      for (auto modit = crate.modules.begin(); modit != crate.modules.end(); modit++) {         
         CAENHV::MODULE mod = (*modit);

         std::map<CAENHV::ParamBase*, ParamOdbInfo> all_slot_params;
         all_slot_params.insert(odb_info_slot_settings[mod.slot].begin(), odb_info_slot_settings[mod.slot].end());

         for (auto it = all_slot_params.begin(); it != all_slot_params.end(); it++) {
            CAENHV::ParamBase* param = it->first;

            // No need to call check_change() if flag is already false
            if (!param->change_was_recently_sent) {
               continue;
            }

            // check_change() will update change_was_recently_sent flag of the parameter if the
            // new value is reported back as expected.
            switch (param->type) {
               case CAENHV::ParamType::Numeric:
                  check_change((CAENHV::ParamFloatBase*)param, it->second, mod.slot);
                  break;
               case CAENHV::ParamType::Binary:
               case CAENHV::ParamType::BoardStatus:
               case CAENHV::ParamType::ChanStatus:
               case CAENHV::ParamType::Enum:
               case CAENHV::ParamType::OnOff:
                  check_change((CAENHV::ParamUnsignedBase*)param, it->second, mod.slot);
                  break;
               case CAENHV::ParamType::ChanName:
                  check_change((CAENHV::ParamChanName*)param, it->second, mod.slot);
                  break;
               default:
                  cm_msg(MERROR, __FUNCTION__, "Unhandled param type %d when checking synced data", param->type);
                  break;
            }

            if (!param->change_was_recently_sent) {
               continue;
            }

            any_unchecked = true;
         }
      }

      if (!any_unchecked) {
         break;
      }

      ss_sleep(10);
   }

   printf("Took %dms for crate to report back all the changes we sent\n", (int)(delta_secs*1000));
   return SUCCESS;
}

void CaenHVFE::check_change(CAENHV::ParamFloatBase* param, ParamOdbInfo& odb_info, int slot) {
   INT num_values = odb_info.odb_num_values;
   float sent_values[num_values];

   for (int i = 0; i < num_values; i++) {
      sent_values[i] = cached_float_odb_change_sent[slot][param][i];
   }
   
   if (!has_new_value(sent_values, param, num_values)) {
      param->change_was_recently_sent = false;
   }
}

void CaenHVFE::check_change(CAENHV::ParamUnsignedBase* param, ParamOdbInfo& odb_info, int slot) {
   INT num_values = odb_info.odb_num_values;
   DWORD sent_values[num_values];

   for (int i = 0; i < num_values; i++) {
      sent_values[i] = cached_unsigned_odb_change_sent[slot][param][i];
   }
   
   if (!has_new_value(sent_values, param, num_values)) {
      param->change_was_recently_sent = false;
   }
}

void CaenHVFE::check_change(CAENHV::ParamChanName* param, ParamOdbInfo& odb_info, int slot) {
   INT num_values = odb_info.odb_num_values;
   std::vector<std::string> sent_values = cached_string_odb_change_sent[slot][param];

   if (!has_new_value(sent_values, param, num_values)) {
      param->change_was_recently_sent = false;
   }
}

INT CaenHVFE::add_to_event(char* pevent, CAENHV::ParamFloatBase* param, ParamOdbInfo& odb_info, std::string bankname) {
   if (odb_info.odb_type != TID_FLOAT) {
      cm_msg(MERROR, __FUNCTION__, "Not implemented: Float param with data type %d", odb_info.odb_type);
      return FE_ERR_DRIVER;
   }

   CAENHV::ParamFloatBase* param_f = (CAENHV::ParamFloatBase*) param;
   float* pdata_f;
   bk_create(pevent, bankname.c_str(), TID_FLOAT, (void **)&pdata_f);

   for (int i = 0; i < odb_info.odb_num_values; i++) {
      *pdata_f++ = param_f->value[i];
   }

   bk_close(pevent, pdata_f);
   return SUCCESS;
}

INT CaenHVFE::add_to_event(char* pevent, CAENHV::ParamUnsignedBase* param, ParamOdbInfo& odb_info, std::string bankname) {
   CAENHV::ParamUnsignedBase* param_u = (CAENHV::ParamUnsignedBase*) param;

   switch (odb_info.odb_type) {
      case TID_DWORD:
         DWORD* pdata_d;
         bk_create(pevent, bankname.c_str(), TID_DWORD, (void **)&pdata_d);

         for (int i = 0; i < odb_info.odb_num_values; i++) {
            *pdata_d++ = param_u->value[i];
         }

         bk_close(pevent, pdata_d);
         break;
      case TID_BOOL:
         BOOL* pdata_b;
         bk_create(pevent, bankname.c_str(), TID_BOOL, (void **)&pdata_b);

         for (int i = 0; i < odb_info.odb_num_values; i++) {
            *pdata_b++ = param_u->value[i];
         }

         bk_close(pevent, pdata_b);
         break;
      default:
         cm_msg(MERROR, __FUNCTION__, "Not implemented: Unsigned param with data type %d", odb_info.odb_type);
         return FE_ERR_DRIVER;
   }

   return SUCCESS;
}

INT CaenHVFE::sync_change(CAENHV::ParamFloatBase* param, ParamOdbInfo& odb_info, int slot, bool to_odb) {
   std::string odb_name = odb_info.odb_name;
   INT odb_type = odb_info.odb_type;
   INT num_values = odb_info.odb_num_values;
   INT status;
   CAENHVRESULT result;
   INT size;
   float data[num_values];
   HNDLE odb_handle = param->is_writable() ? hSlotSettings[slot] : hSlotStatus[slot];

   switch(odb_type) {
      case TID_FLOAT:
         size = sizeof(float) * num_values;
         status = db_get_value(hDB, odb_handle, odb_name.c_str(), data, &size, TID_FLOAT, FALSE);

         if (has_new_value(data, param, num_values)) {
            if (to_odb) {
               // Write to ODB
               status = db_set_value(hDB, odb_handle, odb_name.c_str(), param->value, sizeof(float) * num_values, num_values, TID_FLOAT);

               if (status != SUCCESS) {
                  cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to ODB", odb_name.c_str(), slot);
                  return FE_ERR_ODB;
               }
            } else {
               // Write to crate
               result = param->write(data, num_values);

               cached_float_odb_change_sent[slot][param] = std::vector<float>(data, data + num_values);

               if (result != CAENHV_OK) {
                  cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to crate", odb_name.c_str(), slot);
                  return FE_ERR_HW;
               }
            }
         }

         return SUCCESS;
      default:
         cm_msg(MERROR, __FUNCTION__, "Not implemented: ParamFloatBase for ODB type %d", odb_type);
         return FE_ERR_DRIVER;
   }
}

INT CaenHVFE::sync_change(CAENHV::ParamUnsignedBase* param, ParamOdbInfo& odb_info, int slot, bool to_odb) {
   std::string odb_name = odb_info.odb_name;
   INT odb_type = odb_info.odb_type;
   INT num_values = odb_info.odb_num_values;
   INT status;
   CAENHVRESULT result;
   INT size;
   DWORD data_dword[num_values];
   unsigned data[num_values];
   HNDLE odb_handle = param->is_writable() ? hSlotSettings[slot] : hSlotStatus[slot];
   bool changed = false;

   switch(odb_type) {
      case TID_BOOL:
      case TID_DWORD:
         size = sizeof(DWORD) * num_values;
         status = db_get_value(hDB, odb_handle, odb_name.c_str(), data_dword, &size, odb_type, FALSE);

         if (has_new_value(data_dword, param, num_values)) {
            changed = true;
            if (to_odb) {
               // Write to ODB. Convert from crate's unsigned to appropriate ODB type.
               DWORD data[num_values];

               for (int i = 0; i < num_values; i++) {
                  data[i] = param->value[i];
               }

               status = db_set_value(hDB, odb_handle, odb_name.c_str(), data, sizeof(DWORD) * num_values, num_values, odb_type);

               if (status != SUCCESS) {
                  cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to ODB", odb_name.c_str(), slot);
                  return FE_ERR_ODB;
               }
            } else {
               // Write to crate. Convert from ODB type to unsigned before sending to crate.
               for (int i = 0; i < num_values; i++) {
                  data[i] = data_dword[i];
               }

               result = param->write(data, num_values);

               cached_unsigned_odb_change_sent[slot][param] = std::vector<unsigned>(data, data + num_values);

               if (result != CAENHV_OK) {
                  cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to crate", odb_name.c_str(), slot);
                  return FE_ERR_HW;
               }
            }
         }
         break;
      default:
         cm_msg(MERROR, __FUNCTION__, "Not implemented: ParamUnsignedBase for ODB type %d", odb_type);
         return FE_ERR_DRIVER;
   }

   if (changed) {
      // Extra params explaining status codes.
      if (param->type == CAENHV::BoardStatus) {
         std::string odb_string_name = odb_name + " String";
         CAENHV::ParamBoardStatus* param_cast = (CAENHV::ParamBoardStatus*)param;
         char val[255];
         snprintf(val, 255, "%s", param_cast->explain_bits().c_str());
         status = db_set_value_index(hDB, odb_handle, odb_string_name.c_str(), val, sizeof(char) * 255, 0, TID_STRING, FALSE);

         if (status != SUCCESS) {
            cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to ODB", odb_string_name.c_str(), slot);
            return FE_ERR_ODB;
         }
      }

      if (param->type == CAENHV::ChanStatus) {
         std::string odb_string_name = odb_name + " String";
         CAENHV::ParamChanStatus* param_cast = (CAENHV::ParamChanStatus*)param;

         for (int i = 0; i < num_values; i++) {
            char val[255];
            snprintf(val, 255, "%s", param_cast->explain_bits(i).c_str());
            status = db_set_value_index(hDB, odb_handle, odb_string_name.c_str(), val, sizeof(char) * 255, i, TID_STRING, FALSE);

            if (status != SUCCESS) {
               cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to ODB", odb_string_name.c_str(), slot);
               return FE_ERR_ODB;
            }
         }
      }
   }

   return SUCCESS;
}

INT CaenHVFE::sync_change(CAENHV::ParamChanName* param, ParamOdbInfo& odb_info, int slot, bool to_odb) {
   std::string odb_name = odb_info.odb_name;
   INT odb_type = odb_info.odb_type;
   INT num_values = odb_info.odb_num_values;

   if (odb_type != TID_STRING) {
      return FE_ERR_ODB;
   }

   INT status;
   CAENHVRESULT result;
   std::vector<std::string> new_values;
   HNDLE odb_handle = param->is_writable() ? hSlotSettings[slot] : hSlotStatus[slot];

   for (int i = 0; i < num_values; i++) {
      std::string new_str;
      status = db_get_value_string(hDB, odb_handle, odb_name.c_str(), i, &new_str, FALSE);
      new_values.push_back(new_str);
   }

   if (has_new_value(new_values, param, num_values)) {
      if (to_odb) {
         // Write to ODB
         for (int i = 0; i < num_values; i++) {
            char val[32];
            snprintf(val, 32, "%s", param->value[i].c_str());
            status = db_set_value_index(hDB, odb_handle, odb_name.c_str(), val, sizeof(char) * 32, i, TID_STRING, FALSE);

            if (status != SUCCESS) {
               cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to ODB", odb_name.c_str(), slot);
               return FE_ERR_ODB;
            }
         }
      } else {
         // Write to crate
         result = param->write(&new_values, num_values);
               
         cached_string_odb_change_sent[slot][param] = new_values;

         if (result != CAENHV_OK) {
            cm_msg(MERROR, __FUNCTION__, "Unable to write new %s values for slot %d to crate", odb_name.c_str(), slot);
            return FE_ERR_HW;
         }
      }
   }

   return SUCCESS;
}


INT CaenHVFE::frontend_init(std::string _eqp_name, HNDLE _hDB, CAENHV_SYSTEM_TYPE_t _sys_type) {
   eqp_name = _eqp_name;
   sys_type = _sys_type;
   hDB = _hDB;

   // Set up global settings in ODB.
   if (create_global_records() != SUCCESS) {
      return FE_ERR_ODB;
   }

   if (open_global_records() != SUCCESS) {
      return FE_ERR_ODB;
   }

   // Connect to the crate and read current values
   if (init_crate() != SUCCESS) {
      return FE_ERR_HW;
   }

   // Create ODB settings/status dir for each slot
   if (create_slot_records() != SUCCESS) {
      return FE_ERR_ODB;
   }

   // Ensure values in DB are correct
   if (write_state_to_odb() != SUCCESS) {
      return FE_ERR_ODB;
   }

   if (open_slot_records() != SUCCESS) {
      return FE_ERR_ODB;
   }

   return SUCCESS;
}

INT CaenHVFE::frontend_exit() {
   return SUCCESS;
}

void CaenHVFE::odb_global_settings_changed_callback() {
   if (strcmp(settings.hostname, prev_settings.hostname) != 0 ||
       strcmp(settings.username, prev_settings.username) != 0 ||
       strcmp(settings.password, prev_settings.password) != 0) {
      cm_msg(MERROR, __FUNCTION__, "Restart frontend to pick up the new crate settings!");
   }

   prev_settings = settings;
}

INT CaenHVFE::init_crate() {
   CAENHVRESULT status = crate.init(sys_type, settings.hostname, settings.username, settings.password);

   if (status != CAENHV_OK) {
      cm_msg(MERROR, __FUNCTION__, "Fatal error: can't connect to CAEN crate. Exiting.");
      return FE_ERR_HW;
   }

   status = crate.read_all_param_values();

   if (status != CAENHV_OK) {
      cm_msg(MERROR, __FUNCTION__, "Fatal error: couldn't read CAEN state. Exiting.");
      return FE_ERR_HW;
   }

   return SUCCESS;
}

void CaenHVFE::odb_slot_settings_changed_callback() {
   if (settings.readonly) {
      //cm_msg(MERROR, __FUNCTION__, "Warning: frontend is in 'read-only' mode - will not send changes to crate.");
   } else {
      changes_pending = true;
   }
}

INT CaenHVFE::create_global_records() {
   INT status, size;

   // Find the base dir
   char base[256];
   sprintf(base, "/Equipment/%s", eqp_name.c_str());

   status = db_find_key(hDB, 0, base, &hBase);

   if (status == DB_NO_KEY) {
      cm_msg(MERROR, __FUNCTION__, "Unable to find base dir for this FE! Abort.");
      return status;
   }

   // Create settings dir if needed
   status = db_find_key(hDB, hBase, "Settings", &hSettings);

   if (status == DB_NO_KEY) {
     status = db_create_key(hDB, hBase, "Settings", TID_KEY);
     if (status != DB_SUCCESS) {
       cm_msg(MERROR, __FUNCTION__, "Unable to create settings key! Abort.");
       return status;
     }

     status = db_find_key(hDB, hBase, "Settings", &hSettings);
     if (status != DB_SUCCESS) {
       cm_msg(MERROR, __FUNCTION__, "Unable to find settings key! Abort.");
       return status;
     }
   }

   // Create status dir if needed
   status = db_find_key(hDB, hBase, "Status", &hStatus);

   if (status == DB_NO_KEY) {
     status = db_create_key(hDB, hBase, "Status", TID_KEY);
     if (status != DB_SUCCESS) {
       cm_msg(MERROR, __FUNCTION__, "Unable to create status key! Abort.");
       return status;
     }

     status = db_find_key(hDB, hBase, "Status", &hStatus);
     if (status != DB_SUCCESS) {
       cm_msg(MERROR, __FUNCTION__, "Unable to find status key! Abort.");
       return status;
     }
   }

   // Create global settings if needed, then open record
   db_check_record(hDB, hSettings, "Global", GLOBAL_SETTINGS_STR, TRUE);
   db_find_key(hDB, hSettings, "Global", &hGlobalSettings);

   size = sizeof(settings);
   db_get_record(hDB, hGlobalSettings, &settings, &size, 0);

   if (settings.hostname[0] == 0) {
      cm_msg(MERROR, __FUNCTION__, "Set CAEN HV hostname in ODB, then start frontend again.");
      return FE_ERR_ODB;
   }

   prev_settings = settings;
   return SUCCESS;
}

INT CaenHVFE::open_global_records() {
   INT status;
   INT size = sizeof(settings);
   status = db_open_record(hDB, hGlobalSettings, &settings, size, MODE_READ, global_settings_update_helper, this);

   if (status != DB_SUCCESS) {
     cm_msg(MERROR, __FUNCTION__, "Fatal error: cannot make hotlink to global settings. Exiting.");
     return status;
   }

   return SUCCESS;
}

INT CaenHVFE::create_slot_records() {
   INT status, size;

   for (auto it = crate.modules.begin(); it != crate.modules.end(); it++) {
      CAENHV::MODULE mod = (*it);
      char slot_name[20];
      sprintf(slot_name, "Slot %d", mod.slot);

      status = db_find_key(hDB, hSettings, slot_name, &(hSlotSettings[mod.slot]));
      bool settings_dir_exists = (status == SUCCESS);
      status = db_find_key(hDB, hStatus, slot_name, &(hSlotStatus[mod.slot]));
      bool status_dir_exists = (status == SUCCESS);

      if (mod.present) {
         // Module present - create settings/status dirs if needed
         if (!settings_dir_exists) {
            status = db_create_key(hDB, hSettings, slot_name, TID_KEY);
            status = db_find_key(hDB, hSettings, slot_name, &(hSlotSettings[mod.slot]));
         }
         if (!status_dir_exists) {
            status = db_create_key(hDB, hStatus, slot_name, TID_KEY);
            status = db_find_key(hDB, hStatus, slot_name, &(hSlotStatus[mod.slot]));
         }

         // Get structure that should be created
         std::pair<std::string, std::string> slot_strings = create_odb_slot_strings(mod.slot);

         // Create structures
         status = db_check_record(hDB, hSettings, slot_name, slot_strings.first.c_str(), TRUE);

         if (status != SUCCESS) {
            cm_msg(MERROR, __FUNCTION__, "Fatal error: couldn't create settings struct for slot %d. Exiting.", mod.slot);
            return status;
         }

         status = db_check_record(hDB, hStatus, slot_name, slot_strings.second.c_str(), TRUE);

         if (status != SUCCESS) {
            cm_msg(MERROR, __FUNCTION__, "Fatal error: couldn't create status struct for slot %d. Exiting.", mod.slot);
            return status;
         }

         // Always update model metadata at init
         db_set_value_string(hDB, hSlotStatus[mod.slot], "Model", &mod.model);
         db_set_value_string(hDB, hSlotStatus[mod.slot], "Description", &mod.description);
         db_set_value(hDB, hSlotStatus[mod.slot], "Serial number", &mod.serial_number, sizeof(INT), 1, TID_INT);
         db_set_value(hDB, hSlotStatus[mod.slot], "Firmware version", &mod.firmware_version, sizeof(float), 1, TID_FLOAT);
         db_set_value(hDB, hSlotStatus[mod.slot], "Num channels", &mod.num_channels, sizeof(INT), 1, TID_INT);
      } else {
         // Module not present - delete settings/status dirs
         if (settings_dir_exists) {
            status = db_delete_key(hDB, hSlotSettings[mod.slot], FALSE);
         }
         if (status_dir_exists) {
            status = db_delete_key(hDB, hSlotStatus[mod.slot], FALSE);
         }
         hSlotSettings.erase(mod.slot);
         hSlotStatus.erase(mod.slot);
      }
   }

   return SUCCESS;
}

INT CaenHVFE::open_slot_records() {
   INT status;

   for (auto it = hSlotSettings.begin(); it != hSlotSettings.end(); it++) {
      // Open with NULL pointer - we'll enumerate contents when it's time to write to device.
      status = db_open_record(hDB, it->second, NULL, 0, MODE_READ, slot_settings_update_helper, this);

      if (status != DB_SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Fatal error: cannot make hotlink to slot %d settings. Exiting.", it->first);
        return status;
      }
   }

   return SUCCESS;
}

std::string odb_type_to_string(INT odb_type) {
   switch (odb_type) {
      case TID_BYTE:
         return "BYTE";
      case TID_SBYTE:
         return "SBYTE";
      case TID_CHAR:
         return "CHAR";
      case TID_WORD:
         return "WORD";
      case TID_SHORT:
         return "SHORT";
      case TID_DWORD:
         return "DWORD";
      case TID_INT:
         return "INT";
      case TID_BOOL:
         return "BOOL";
      case TID_FLOAT:
         return "FLOAT";
      case TID_DOUBLE:
         return "DOUBLE";
      case TID_BITFIELD:
         return "BITFIELD";
      case TID_STRING:
         return "STRING";
      default:
         return "";
   }
}

INT default_odb_type_for_param(CAENHV::ParamBase* param) {
   switch (param->type) {
      case CAENHV::Numeric:
         return TID_FLOAT;
      case CAENHV::OnOff:
         return TID_BOOL;
      case CAENHV::ChanStatus:
      case CAENHV::BoardStatus:
      case CAENHV::Binary:
      case CAENHV::Enum:
         return TID_DWORD;
      case CAENHV::ChanName:
      case CAENHV::String:
         return TID_STRING;
      default:
         return -1;
   }
}

bool name_comp(CAENHV::ParamBase* i, CAENHV::ParamBase* j) {
   return i->name.compare(j->name) < 0;
}

std::pair<std::string, std::string> CaenHVFE::create_odb_slot_strings(int slot) {
   std::stringstream sett, stat, meta;

   CAENHV::MODULE& mod = crate.get_module(slot);

   // Metadata from module
   meta << "Model = STRING : [32] " << mod.model << "\n";
   meta << "Description = STRING : [128] " << mod.description << "\n";
   meta << "Serial number = INT : " << mod.serial_number << "\n";
   meta << "Firmware version = FLOAT : " << mod.firmware_version << "\n";
   meta << "Num channels = INT : " << mod.num_channels << "\n";

   // Parameters from module
   std::vector<CAENHV::ParamBase*> all_params;
   all_params.insert(all_params.begin(), mod.board_params.begin(), mod.board_params.end());
   all_params.insert(all_params.begin(), mod.channel_params.begin(), mod.channel_params.end());

   // Sort parameters by name
   std::sort(all_params.begin(), all_params.end(), name_comp);

   for (auto it = all_params.begin(); it != all_params.end(); it++) {
      CAENHV::ParamBase* param = *it;

      if (!param->is_readable()) {
         continue;
      }

      if (ignore_params.find(param->name) != ignore_params.end()) {
         continue;
      }

      ParamOdbInfo odb_info;
      std::stringstream this_var;
      std::string odb_name = param->name;

      if (odb_aliases.find(param->name) != odb_aliases.end()) {
         // User over-ride (e.g. changing V0Set to Demand).
         odb_name = odb_aliases[param->name];
      }

      if (param->type == CAENHV::Numeric) {
         // Add units.
         CAENHV::ParamNumeric* casted = (CAENHV::ParamNumeric*) param;
         if (casted->units.size()) {
            std::string units = casted->units;
            while (units.find("/") != std::string::npos) {
               units = units.replace(units.find("/"), 1, "p");
            }

            odb_name += " (" + units + ")";
         }
      }

      if (param->type == CAENHV::OnOff) {
         // Note the on/off state names
         CAENHV::ParamOnOff* casted = (CAENHV::ParamOnOff*) param;
         meta << "OnState " << odb_name << " = STRING : [32] " << casted->onstate << "\n";
         meta << "OffState " << odb_name << " = STRING : [32] " << casted->offstate << "\n";
      }

      INT odb_type = default_odb_type_for_param(param);

      if (odb_types.find(param->name) != odb_types.end()) {
         odb_type = odb_types[param->name];
      }

      std::string odb_type_str = odb_type_to_string(odb_type);

      if (odb_type_str.size() == 0) {
         throw std::out_of_range("Unhandled ODB type");
      }

      this_var << odb_name << " = " << odb_type_str;

      if (param->is_board_param) {
         this_var << " : ";
         if (odb_type == TID_STRING) {
            this_var << "[32]";
         }
         this_var << param->value_to_text(0) << " \n";
      } else {
         this_var << "[" << mod.num_channels << "] : \n";

         for (int chan_idx = 0; chan_idx < mod.num_channels; chan_idx++) {
            if (odb_type == TID_STRING) {
               this_var << "[32] ";
            } else {
               this_var << "[" << chan_idx << "] ";
            }
            this_var << param->value_to_text(chan_idx) << " \n";
         }
      }

      odb_info.odb_name = odb_name;
      odb_info.odb_type = odb_type;
      odb_info.odb_num_values = (param->is_board_param ? 1 : mod.num_channels);

      if (param->type == CAENHV::ChanStatus || param->type == CAENHV::BoardStatus) {
         // Extra params explaining the status codes
         this_var << odb_name << " String = STRING";
         if (param->is_board_param) {
            this_var << " : [255] \n";
         } else {
            this_var << "[" << mod.num_channels << "] : \n";

            for (int chan_idx = 0; chan_idx < mod.num_channels; chan_idx++) {
               this_var << "[255] \n";
            }
         }
      }

      if (param->is_writable()) {
         sett << this_var.str();
         odb_info_slot_settings[slot][param] = odb_info;
      } else {
         stat << this_var.str();
         odb_info_slot_status[slot][param] = odb_info;
      }
   }

   // Combine metadata and status data currently
   meta << stat.str();

   return std::make_pair(sett.str(), meta.str());
}


bool CaenHVFE::has_new_value(float* new_val, CAENHV::ParamFloatBase* param, int num_channels) {
   for (int i = 0; i < num_channels; i++) {
      if (fabs(new_val[i] - param->value[i]) > 1e-6) {
         return true;
      }
   }

   return false;
}

bool CaenHVFE::has_new_value(DWORD* new_val, CAENHV::ParamUnsignedBase* param, int num_channels) {
   for (int i = 0; i < num_channels; i++) {
      if (new_val[i] != param->value[i]) {
         return true;
      }
   }

   return false;
}

bool CaenHVFE::has_new_value(std::vector<std::string>& new_val, CAENHV::ParamChanName* param, int num_channels) {
   for (int i = 0; i < num_channels; i++) {
      if (new_val[i].compare(param->value[i]) != 0) {
         return true;
      }
   }

   return false;
}
