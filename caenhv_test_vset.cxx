#include "caenhvwrapper_cxx.h"
#include "system_type.h"
#include <string>
#include <cstring>
#include <cmath>
#include <unistd.h>
#include <sys/time.h>

// Test script for changing the voltage on a single channel on a single module,
// and seeing how long it takes for the module/crate to report back the new value.
//
// Some modules take > 100ms to report voltage changes. This must be accounted for
// in the midas frontend, as we do bi-directional syncing of settings. We must wait
// at least N ms after we write a new value before we read back all the settings again,
// otherwise we run the risk of doing: 
// * User changes value in ODB from OLD to NEW
// * We detect change to ODB and write NEW value to board
// * We read all values from board; board still reports the OLD value
// * We write OLD value to ODB, overwriting the change the user made
// * We detect change to ODB and write OLD value to board
// * User's change has been lost - both ODB and board are at OLD value

int main(int argc, char* argv[]) {
   CAENHV::CRATE crate;

   if (argc != 7) {
      printf("Usage: ./caenhv_test_vset <ip> <username> <password> <module> <channel> <new_voltage>\n");
      exit(1);
   }

   std::string ip(argv[1]);
   std::string user(argv[2]);
   std::string pass(argv[3]);
   int mod_num = atoi(argv[4]);
   int chan = atoi(argv[5]);
   float new_volt;
   sscanf(argv[6], "%f", &new_volt);

   CAENHVRESULT status = crate.init(SYSTEM_TYPE, ip, user, pass);

   if (status != CAENHV_OK) {
      CAENHV::print_hv_error("Failed to init", status);
      return 1;
   }

    CAENHV::MODULE& mod = crate.modules[mod_num];

    if (!mod.present) {
        printf("Slot %d: Not present\n", mod.slot);
        return 1;
    }

   status = crate.read_all_param_values();

   if (status != CAENHV_OK) {
      CAENHV::print_hv_error("Failed to read param values", status);
      return 1;
   }

   CAENHV::ParamBase* param = NULL;

   for (auto it = mod.channel_params.begin(); it != mod.channel_params.end(); it++) {
      if ((*it)->name == "V0Set") {
         param = *it;
      }
   }
   
   if (!param) {
      printf("Failed to find V0Set for module %d\n", mod_num);
      return 1;
   }

   CAENHV::ParamFloatBase* cast_param = (CAENHV::ParamFloatBase*)param;
   float voltages[mod.num_channels];
   memcpy(voltages, cast_param->value, mod.num_channels * sizeof(float));
   printf("Current voltage for chan %d is %f. Setting to %f.\n", chan, voltages[chan], new_volt);
   
   voltages[chan] = new_volt;

   cast_param->write(voltages, mod.num_channels);

   timeval start;
   gettimeofday(&start, NULL);
   bool seen_change = false;
   double delta_secs = 0;
   int max_delta_secs = 3;
   
   while (delta_secs < max_delta_secs) {
      timeval now;
      gettimeofday(&now, NULL);
      delta_secs = (now.tv_sec - start.tv_sec) + 1e-6 * (now.tv_usec - start.tv_usec);

      crate.read_all_param_values();

      if (fabs(cast_param->value[chan] - new_volt) < 1e-5) {
         printf("Took approx %dms for crate to report the change\n", (int)(delta_secs*1000));
         seen_change = true;
         break;
      }

      usleep(1000);
   }

   if (!seen_change) {
      printf("Read V0Set is %fV after waiting %d seconds!\n", cast_param->value[chan], max_delta_secs);
   }

   return 0;
}
