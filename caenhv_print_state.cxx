#include "caenhvwrapper_cxx.h"
#include "system_type.h"
#include <string>
#include <set>

// This script does not use midas ODB at all.

int main(int argc, char* argv[]) {
   CAENHV::CRATE crate;

   if (argc < 4) {
      printf("Usage: ./caenhv_print_state <ip> <username> <password>\n");
      exit(1);
   }

   std::string ip(argv[1]);
   std::string user(argv[2]);
   std::string pass(argv[3]);

   CAENHVRESULT status = crate.init(SYSTEM_TYPE, ip, user, pass);

   if (status != CAENHV_OK) {
      CAENHV::print_hv_error("Failed to init", status);
      return 1;
   }

   std::set<std::string> all_par_names;

   for (unsigned int i = 0; i < crate.modules.size(); i++) {
      CAENHV::MODULE& mod = crate.modules[i];

      if (!mod.present) {
         printf("Slot %d: Not present\n", mod.slot);
         continue;
      }

      printf("Slot %d: %s (%s) (serial %d, fw %.2f), %d channels\n", mod.slot, mod.model.c_str(), mod.description.c_str(), mod.serial_number, mod.firmware_version, mod.num_channels);

      printf("\tBoard parameters:\n");
      for (unsigned i = 0; i < mod.board_params.size(); i++) {
         printf("\t\t%s\n", mod.board_params[i]->description().c_str());
      }

      printf("\tChannel parameters:\n");
      for (unsigned i = 0; i < mod.channel_params.size(); i++) {
         printf("\t\t%s\n", mod.channel_params[i]->description().c_str());
         all_par_names.insert(mod.channel_params[i]->name);
      }
   }

   status = crate.read_all_param_values();

   if (status != CAENHV_OK) {
      CAENHV::print_hv_error("Failed to read param values", status);
      return 1;
   }

   printf("\n");
   printf("Slot");

   for (std::set<std::string>::iterator it = all_par_names.begin(); it != all_par_names.end(); it++) {
      printf("\t%s", (*it).c_str());

      if (it->compare("ChName") == 0) {
         printf("        ");
      }
   }

   printf("\n");

   for (unsigned int i = 0; i < crate.modules.size(); i++) {
      CAENHV::MODULE& mod = crate.modules[i];

      if (!mod.present) {
         continue;
      }

      for (unsigned chan_idx = 0; chan_idx < mod.num_channels; chan_idx++) {
         printf("%d\t", mod.slot);

         for (std::set<std::string>::iterator it = all_par_names.begin(); it != all_par_names.end(); it++) {
            std::string par_name = *it;
            if (mod.has_channel_param(par_name)) {
               printf("%s\t", mod.get_channel_param(par_name)->value_to_text(chan_idx, true).c_str());
            } else {
               printf("\t");
            }
         }

         printf("\n");
      }
   }


   return 0;
}
