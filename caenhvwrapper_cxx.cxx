#include "caenhvwrapper_cxx.h"
#include "CAENHVWrapper.h"
#include <vector>
#include <string>
#include <map>
#include <string.h>
#include <stdexcept>

namespace CAENHV {

   std::string param_type_to_text(ParamType code) {
      switch (code) {
         case Numeric:
            return "Numeric";
         case OnOff:
            return "OnOff";
         case ChanStatus:
            return "ChanStatus";
         case BoardStatus:
            return "BoardStatus";
         case Binary:
            return "Binary";
         case String:
            return "String";
         case Enum:
            return "Enum";
         case ChanName:
            return "ChanName";
         default:
            return "???";
      }
   }

   std::string param_mode_to_text(ParamMode code) {
      switch (code) {
         case RdOnly:
            return "R";
         case WrOnly:
            return "W";
         case RdWr:
            return "RW";
         default:
            return "??";
      }
   }

   std::string error_code_to_text(int code) {
      switch (code) {
         case CAENHV_OK:
            return "CAENHV_OK";
         case CAENHV_SYSERR:
            return "CAENHV_SYSERR";
         case CAENHV_WRITEERR:
            return "CAENHV_WRITEERR";
         case CAENHV_READERR:
            return "CAENHV_READERR";
         case CAENHV_TIMEERR:
            return "CAENHV_TIMEERR";
         case CAENHV_DOWN:
            return "CAENHV_DOWN";
         case CAENHV_NOTPRES:
            return "CAENHV_NOTPRES";
         case CAENHV_SLOTNOTPRES:
            return "CAENHV_SLOTNOTPRES";
         case CAENHV_NOSERIAL:
            return "CAENHV_NOSERIAL";
         case CAENHV_MEMORYFAULT:
            return "CAENHV_MEMORYFAULT";
         case CAENHV_OUTOFRANGE:
            return "CAENHV_OUTOFRANGE";
         case CAENHV_EXECCOMNOTIMPL:
            return "CAENHV_EXECCOMNOTIMPL";
         case CAENHV_GETPROPNOTIMPL:
            return "CAENHV_GETPROPNOTIMPL";
         case CAENHV_SETPROPNOTIMPL:
            return "CAENHV_SETPROPNOTIMPL";
         case CAENHV_PROPNOTFOUND:
            return "CAENHV_PROPNOTFOUND";
         case CAENHV_EXECNOTFOUND:
            return "CAENHV_EXECNOTFOUND";
         case CAENHV_NOTSYSPROP:
            return "CAENHV_NOTSYSPROP";
         case CAENHV_NOTGETPROP:
            return "CAENHV_NOTGETPROP";
         case CAENHV_NOTSETPROP:
            return "CAENHV_NOTSETPROP";
         case CAENHV_NOTEXECOMM:
            return "CAENHV_NOTEXECOMM";
         case CAENHV_SYSCONFCHANGE:
            return "CAENHV_SYSCONFCHANGE";
         case CAENHV_PARAMPROPNOTFOUND:
            return "CAENHV_PARAMPROPNOTFOUND";
         case CAENHV_PARAMNOTFOUND:
            return "CAENHV_PARAMNOTFOUND";
         case CAENHV_NODATA:
            return "CAENHV_NODATA";
         case CAENHV_DEVALREADYOPEN:
            return "CAENHV_DEVALREADYOPEN";
         case CAENHV_TOOMANYDEVICEOPEN:
            return "CAENHV_TOOMANYDEVICEOPEN";
         case CAENHV_INVALIDPARAMETER:
            return "CAENHV_INVALIDPARAMETER";
         case CAENHV_FUNCTIONNOTAVAILABLE:
            return "CAENHV_FUNCTIONNOTAVAILABLE";
         case CAENHV_SOCKETERROR:
            return "CAENHV_SOCKETERROR";
         case CAENHV_COMMUNICATIONERROR:
            return "CAENHV_COMMUNICATIONERROR";
         case CAENHV_NOTYETIMPLEMENTED:
            return "CAENHV_NOTYETIMPLEMENTED";
         case CAENHV_CONNECTED:
            return "CAENHV_CONNECTED";
         case CAENHV_NOTCONNECTED:
            return "CAENHV_NOTCONNECTED";
         case CAENHV_OS:
            return "CAENHV_OS";
         case CAENHV_LOGINFAILED:
            return "CAENHV_LOGINFAILED";
         case CAENHV_LOGOUTFAILED:
            return "CAENHV_LOGOUTFAILED";
         case CAENHV_LINKNOTSUPPORTED:
            return "CAENHV_LINKNOTSUPPORTED";
         case CAENHV_USERPASSFAILED:
            return "CAENHV_USERPASSFAILED";
         default:
            return "UNKNOWN_CODE";
      }
   }

   std::string chan_status_bit_to_text(int code) {
      switch(code) {
         case ParamChanStatus::On:
            return "On";
         case ParamChanStatus::RampingUp:
            return "RampingUp";
         case ParamChanStatus::RampingDown:
            return "RampingDown";
         case ParamChanStatus::OverCurrent:
            return "OverCurrent";
         case ParamChanStatus::OverVoltage:
            return "OverVoltage";
         case ParamChanStatus::UnderVoltage:
            return "UnderVoltage";
         case ParamChanStatus::ExternalTrip:
            return "ExternalTrip";
         case ParamChanStatus::MaxV:
            return "MaxV";
         case ParamChanStatus::ExternalDisable:
            return "ExternalDisable";
         case ParamChanStatus::InternalTrip:
            return "InternalTrip";
         case ParamChanStatus::CalibrationError:
            return "CalibrationError";
         case ParamChanStatus::Unplugged:
            return "Unplugged";
         default:
            return "?";
      }
   }

   std::string board_status_bit_to_text(int code) {
      switch(code) {
         case ParamBoardStatus::PowerFail:
            return "PowerFail";
         case ParamBoardStatus::FirmwareChecksumError:
            return "FirmwareChecksumError";
         case ParamBoardStatus::CalibrationErrorHV:
            return "CalibrationErrorHV";
         case ParamBoardStatus::CalibrationErrorTemp:
            return "CalibrationErrorTemp";
         case ParamBoardStatus::UnderTemperature:
            return "UnderTemperature";
         case ParamBoardStatus::OverTemperature:
            return "OverTemperature";
         default:
            return "?";
      }
   }

   void print_hv_error(std::string preamble, int code) {
      printf("%s: %d (%s)\n", preamble.c_str(), code, error_code_to_text(code).c_str());
   }

   ParamBase::ParamBase(bool _is_board_param, int _handle, int _slot, char* param_name, ParamType param_type) {
      CAENHVRESULT retval;
      name = std::string(param_name);
      type = param_type;
      is_board_param = _is_board_param;
      handle = _handle;
      slot = _slot;
      change_was_recently_sent = false;

      if (is_board_param) {
         retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Mode", (void*) &(mode));
      } else {
         retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Mode", (void*) &(mode));
      }
   }

   bool ParamBase::is_writable() {
      return mode == WrOnly || mode == RdWr;
   }

   bool ParamBase::is_readable() {
      return mode == RdOnly || mode == RdWr;
   }

   std::string ParamBase::description() {
      return name + " (" + param_type_to_text(type) + ") (" + param_mode_to_text(mode) + ")";
   }

   ParamFloatBase::ParamFloatBase(bool is_board_param, int handle, int slot, char* param_name, ParamType param_type) :
      ParamBase(is_board_param, handle, slot, param_name, param_type) {
      value = NULL;
   }

   ParamFloatBase::~ParamFloatBase() {
      free(value);
   }

   CAENHVRESULT ParamFloatBase::read(int num_channels) {
      if (!is_readable()) {
         return -60;
      }

      if (value == NULL) {
         value = (float*) malloc(num_channels * sizeof(float));
      }

      CAENHVRESULT retval = CAENHV_OK;

      if (is_board_param) {
         if (num_channels != 1) {
            return -61;
         }

         unsigned short slot_list[1];
         slot_list[0] = slot;

         retval = CAENHV_GetBdParam(handle, 1, slot_list, name.c_str(), value);
      } else {
         unsigned short ch_list[num_channels];

         for (int i = 0; i < num_channels; i++) {
            ch_list[i] = i;
         }

         retval = CAENHV_GetChParam(handle, slot, name.c_str(), num_channels, ch_list, value);
      }

      return retval;
   }

   CAENHVRESULT ParamFloatBase::write(void* new_values, int num_channels) {
      if (!is_writable() || new_values == NULL) {
         return -61;
      }

      CAENHVRESULT retval = CAENHV_OK;
      float* new_casted = (float*)new_values;

      if (is_board_param) {
         if (num_channels != 1) {
            return -61;
         }

         unsigned short slot_list[1];
         slot_list[0] = slot;

         retval = CAENHV_SetBdParam(handle, 1, slot_list, name.c_str(), new_casted);
      } else {
         unsigned short ch_list[1];

         for (int i = 0; i < num_channels; i++) {
            ch_list[0] = i;
            retval = CAENHV_SetChParam(handle, slot, name.c_str(), 1, ch_list, &(new_casted[i]));
         }
      }

      change_was_recently_sent = true;

      return retval;
   }

   std::string ParamFloatBase::value_to_text(int chan_idx, bool fancy) {
      char buf[100];
      if (value) {
         sprintf(buf, "%.2f", value[chan_idx]);
      } else {
         sprintf(buf, "???");
      }
      return buf;
   }

   ParamUnsignedBase::ParamUnsignedBase(bool is_board_param, int handle, int slot, char* param_name, ParamType param_type) :
      ParamBase(is_board_param, handle, slot, param_name, param_type) {
      value = NULL;
   }

   ParamUnsignedBase::~ParamUnsignedBase() {
      free(value);
   }

   CAENHVRESULT ParamUnsignedBase::read(int num_channels) {
      if (!is_readable()) {
         return -60;
      }

      if (value == NULL) {
         value = (unsigned*) malloc(num_channels * sizeof(unsigned));
      }

      CAENHVRESULT retval = CAENHV_OK;

      if (is_board_param) {
         if (num_channels != 1) {
            return -61;
         }

         unsigned short slot_list[1];
         slot_list[0] = slot;

         retval = CAENHV_GetBdParam(handle, 1, slot_list, name.c_str(), value);
      } else {
         unsigned short ch_list[num_channels];

         for (int i = 0; i < num_channels; i++) {
            ch_list[i] = i;
         }

         retval = CAENHV_GetChParam(handle, slot, name.c_str(), num_channels, ch_list, value);
      }

      return retval;
   }

   CAENHVRESULT ParamUnsignedBase::write(void* new_values, int num_channels) {
      // new_values should come from ODB, for example. Should be unsigned* here.
      // We do NOT update the main "value" member as we will do that next time read() is called.
      if (!is_writable() || new_values == NULL) {
         return -61;
      }

      CAENHVRESULT retval = CAENHV_OK;
      unsigned* new_casted = (unsigned*)new_values;

      if (is_board_param) {
         if (num_channels != 1) {
            return -61;
         }

         unsigned short slot_list[1];
         slot_list[0] = slot;

         retval = CAENHV_SetBdParam(handle, 1, slot_list, name.c_str(), new_casted);
      } else {
         unsigned short ch_list[1];

         for (int i = 0; i < num_channels; i++) {
            ch_list[0] = i;
            retval = CAENHV_SetChParam(handle, slot, name.c_str(), 1, ch_list, &(new_casted[i]));
         }
      }

      change_was_recently_sent = true;

      return retval;
   }

   std::string ParamUnsignedBase::value_to_text(int chan_idx, bool fancy) {
      char buf[100];
      if (value) {
         sprintf(buf, "%d", value[chan_idx]);
      } else {
         sprintf(buf, "???");
      }
      return buf;
   }

   ParamChanName::ParamChanName(int handle, int slot, char* par_name) :
     ParamBase(false, handle, slot, par_name, ChanName) {
      mode = RdWr;
   }

   CAENHVRESULT ParamChanName::read(int num_channels) {
      value.clear();
      char (*listNameCh)[MAX_CH_NAME];
      listNameCh = (char (*)[MAX_CH_NAME]) malloc(num_channels * MAX_CH_NAME);

      CAENHVRESULT retval = CAENHV_OK;

      unsigned short ch_list[num_channels];

      for (int i = 0; i < num_channels; i++) {
         ch_list[i] = i;
      }

      retval = CAENHV_GetChName(handle, slot, num_channels, ch_list, listNameCh);

      if (retval != CAENHV_OK) {
         free(listNameCh);
         return retval;
      }

      for (int i = 0; i < num_channels; i++) {
         value.push_back(listNameCh[i]);
      }

      free(listNameCh);
      return retval;
   }

   CAENHVRESULT ParamChanName::write(void* new_values, int num_channels) {
      // new_values should come from ODB, for example. Should be std::vector<std::string> here.
      // We do NOT update the main "value" member as we will do that next time read() is called.
      std::vector<std::string>* new_strings = (std::vector<std::string>*)new_values;

      char chname[MAX_CH_NAME];

      CAENHVRESULT retval = CAENHV_OK;

      unsigned short ch_list[1];

      for (int i = 0; i < num_channels; i++) {
         ch_list[0] = i;

         snprintf(chname, MAX_CH_NAME, "%s", new_strings->at(i).c_str());
         retval = CAENHV_SetChName(handle, slot, 1, ch_list, chname);

         if (retval != CAENHV_OK) {
            return retval;
         }
      }

      change_was_recently_sent = true;
      
      return retval;
   }

   std::string ParamChanName::value_to_text(int chan_idx, bool fancy) {
      std::string retval = "???";

      if (chan_idx < value.size()) {
         retval = value[chan_idx];
      }

      while (retval.size() < MAX_CH_NAME) {
         retval += " ";
      }

      return retval;
   }

   ParamNumeric::ParamNumeric(bool is_board_param, int handle, int slot, char* param_name) :
      ParamFloatBase(is_board_param, handle, slot, param_name, Numeric) {
      CAENHVRESULT retval;
      unsigned short unit;
      short exp;

      if (is_board_param) {
       retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Minval", (void*) &(min_val));
       retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Maxval", (void*) &(max_val));
       retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Unit", (void*) &unit);
       retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Exp", (void*) &exp);
      } else {
       retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Minval", (void*) &(min_val));
       retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Maxval", (void*) &(max_val));
       retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Unit", (void*) &unit);
       retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Exp", (void*) &exp);
      }

      switch (exp) {
       case 3:
          units += "k";
          break;
       case 6:
          units += "M";
          break;
       case -3:
          units += "m";
          break;
       case -6:
          units += "u";
          break;
      }

      switch (unit) {
       case PARAM_UN_AMPERE:
          units += "A";
          break;
       case PARAM_UN_VOLT:
          units += "V";
          break;
       case PARAM_UN_WATT:
          units += "W";
          break;
       case PARAM_UN_CELSIUS:
          units += "C";
          break;
       case PARAM_UN_HERTZ:
          units += "Hz";
          break;
       case PARAM_UN_BAR:
          units += "bar";
          break;
       case PARAM_UN_VPS:
          units += "V/s";
          break;
       case PARAM_UN_SECOND:
          units += "s";
          break;
       case PARAM_UN_RPM:
          units += "RPM";
          break;
      }
   }

   std::string ParamNumeric::description() {
      return ParamBase::description() + " (units '" + units + "')";
   }

   ParamOnOff::ParamOnOff(bool is_board_param, int handle, int slot, char* param_name) :
      ParamUnsignedBase(is_board_param, handle, slot, param_name, OnOff) {
      CAENHVRESULT retval;
      value = NULL;

      if (is_board_param) {
         retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Mode", (void*) &(mode));
         retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Onstate", (void*) onstate);
         retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Offstate", (void*) offstate);
      } else {
         retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Mode", (void*) &(mode));
         retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Onstate", (void*) onstate);
         retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Offstate", (void*) offstate);
      }
   }

   std::string ParamOnOff::value_to_text(int chan_idx, bool fancy) {
      if (value) {
         if (value[chan_idx]) {
            return fancy ? onstate : "y";
         } else {
            return fancy ? offstate : "n";
         }
      }

      return "???";
   }

   ParamChanStatus::ParamChanStatus(int handle, int slot, char* param_name) :
      ParamUnsignedBase(false, handle, slot, param_name, ChanStatus) {
      value = NULL;
   }

   std::string ParamChanStatus::explain_bits(int chan_idx) {
      std::string retval = "";

      if (value) {
         if (!value[chan_idx] & (1<<ParamChanStatus::On)) {
            retval += "Off ";
         }

         for (int i = 0; i < ParamChanStatus::MaxChanParamBit; i++) {
            if (value[chan_idx] & (1<<i)) {
               retval += chan_status_bit_to_text(i) + " ";
            }
         }
      }

      return retval;
   }

   ParamBoardStatus::ParamBoardStatus(int handle, int slot, char* param_name) :
      ParamUnsignedBase(true, handle, slot, param_name, BoardStatus) {
      value = NULL;
   }

   std::string ParamBoardStatus::explain_bits() {
      std::string retval = "";

      if (value) {
         for (int i = 0; i < ParamBoardStatus::MaxBoardParamBit; i++) {
            if (value[0] & (1<<i)) {
               retval += board_status_bit_to_text(i) + " ";
            }
            if (value[0] == 0) {
               retval = "OK";
            }
         }
      }

      return retval;
   }

   ParamBinary::ParamBinary(bool is_board_param, int handle, int slot, char* param_name) :
      ParamUnsignedBase(is_board_param, handle, slot, param_name, Binary)  {
      value = NULL;
   }

   ParamEnum::ParamEnum(bool is_board_param, int handle, int slot, char* param_name) :
      ParamUnsignedBase(is_board_param, handle, slot, param_name, Enum) {
      CAENHVRESULT retval;
      value = NULL;
      char* EnumNameList;

      float min_val;
      float max_val;

      if (is_board_param) {
         retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Minval", (void*) &(min_val));
         retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Maxval", (void*) &(max_val));
         retval = CAENHV_GetBdParamProp(handle, slot, param_name, "Enum", (void*) &(EnumNameList));
      } else {
         retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Minval", (void*) &(min_val));
         retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Maxval", (void*) &(max_val));
         retval = CAENHV_GetChParamProp(handle, slot, 0, param_name, "Enum", (void*) &(EnumNameList));
      }

      char (*enum_names)[15] = (char (*)[15])EnumNameList;
      minval = min_val;

      for (int i = min_val; i < max_val; i++) {
         meanings.push_back(std::string(enum_names[i]));
      }
   }

   std::string ParamEnum::value_to_text(int chan_idx, bool fancy) {
      if (value) {
         if (fancy) {
            return meanings.at(value[chan_idx] - minval);
         } else {
            return ParamUnsignedBase::value_to_text(chan_idx, fancy);
         }
      }
      return "";
   }

   void MODULE::free_params() {
      for (unsigned int i = 0; i < board_params.size(); i++) {
         delete board_params[i];
      }

      for (unsigned int i = 0; i < channel_params.size(); i++) {
         delete channel_params[i];
      }
   }

   bool MODULE::has_channel_param(std::string name) {
      return get_channel_param(name) != NULL;
   }

   ParamBase* MODULE::get_channel_param(std::string name) {
      for (unsigned int i = 0; i < channel_params.size(); i++) {
         if (channel_params[i]->name.compare(name) == 0) {
            return channel_params[i];
         }
      }

      return NULL;
   }

   CAENHVRESULT MODULE::init_board_parameters(int handle) {
      CAENHVRESULT retval = CAENHV_OK;
      char* ParNameList;
      retval = CAENHV_GetBdParamInfo(handle, slot, &ParNameList);

      if (retval != CAENHV_OK) {
         return retval;
      }

      char (*par_names)[MAX_PARAM_NAME] = (char (*)[MAX_PARAM_NAME])ParNameList;

      int par_idx = 0;

      while (par_names[par_idx][0] != 0) {
         int par_type;
         retval = CAENHV_GetBdParamProp(handle, slot, par_names[par_idx], "Type", (void*) &par_type);

         if (retval != CAENHV_OK) {
            printf("Failed to read Type of board param %i (%s)\n", par_idx, par_names[par_idx]);
            free(ParNameList);
            return retval;
         }

         ParamBase* param = NULL;

         switch (par_type) {
            case Numeric:
               board_params.push_back(new ParamNumeric(true, handle, slot, par_names[par_idx]));
               break;
            case OnOff:
               board_params.push_back(new ParamOnOff(true, handle, slot, par_names[par_idx]));
               break;
            case ChanStatus:
               return -56;
               break;
            case BoardStatus:
               board_params.push_back(new ParamBoardStatus(handle, slot, par_names[par_idx]));
               break;
            case Binary:
               board_params.push_back(new ParamBinary(true, handle, slot, par_names[par_idx]));
               break;
            case String:
               // String params not yet supported
               printf("String param %s not supported yet\n", par_names[par_idx]);
               break;
            case Enum:
               board_params.push_back(new ParamEnum(true, handle, slot, par_names[par_idx]));
               break;
            default:
               printf("Unexpected Type (%i) of board param %i (%s)\n", par_type, par_idx, par_names[par_idx]);
               return -57;
         }

         par_idx++;
      }

      free(ParNameList);
      return retval;
   }

   CAENHVRESULT MODULE::init_channel_parameters(int handle) {
      // Currently assumes all channels on a module have the same parameters.

      channel_params.clear();

      // Dummy param for reading channel name
      char par_name[20];
      sprintf(par_name, "ChName");
      channel_params.push_back(new ParamChanName(handle, slot, par_name));

      // Real params read from board
      CAENHVRESULT retval = CAENHV_OK;
      char* ParNameList;
      int ParNumber;
      retval = CAENHV_GetChParamInfo(handle, slot, 0, &ParNameList, &ParNumber);

      if (retval != CAENHV_OK) {
         return retval;
      }

      char (*par_names)[MAX_PARAM_NAME] = (char (*)[MAX_PARAM_NAME])ParNameList;

      for (int par_idx = 0; par_idx < ParNumber; par_idx++) {
         int par_type;
         retval = CAENHV_GetChParamProp(handle, slot, 0, par_names[par_idx], "Type", (void*) &par_type);

         if (retval != CAENHV_OK) {
            printf("Failed to read Type of board param %i (%s)\n", par_idx, par_names[par_idx]);
            free(ParNameList);
            return retval;
         }

         switch (par_type) {
            case Numeric:
               channel_params.push_back(new ParamNumeric(false, handle, slot, par_names[par_idx]));
               break;
            case OnOff:
               channel_params.push_back(new ParamOnOff(false, handle, slot, par_names[par_idx]));
               break;
            case ChanStatus:
               channel_params.push_back(new ParamChanStatus(handle, slot, par_names[par_idx]));
               break;
            case BoardStatus:
               return -55;
               break;
            case Binary:
               channel_params.push_back(new ParamBinary(false, handle, slot, par_names[par_idx]));
               break;
            case String:
               // String params not yet supported
               printf("String param %s not supported yet\n", par_names[par_idx]);
               break;
            case Enum:
               channel_params.push_back(new ParamEnum(false, handle, slot, par_names[par_idx]));
               break;
            default:
               printf("Unexpected Type (%i) of channel param %i (%s)\n", par_type, par_idx, par_names[par_idx]);
               return -57;
         }
      }

      free(ParNameList);
      return retval;
   }

   CRATE::CRATE() {
      handle = -1;
      num_slots = 0;
   }

   CAENHVRESULT CRATE::init(CAENHV_SYSTEM_TYPE_t sys_type, std::string ip_addr, std::string username, std::string password) {
      CAENHVRESULT retval = CAENHV_OK;

      int link_type = LINKTYPE_TCPIP;

      retval = CAENHV_InitSystem(sys_type, link_type, (void*)ip_addr.c_str(), username.c_str(), password.c_str(), &handle);

      if (retval != CAENHV_OK) {
         print_hv_error("Error connecting to system", retval);
         return retval;
      }

      unsigned short NrOfSlot, *NrOfChList, *SerNumList;
      char *ModelList, *DescriptionList;
      unsigned char *FmwRelMinList, *FmwRelMaxList;

      retval = CAENHV_GetCrateMap(handle, &NrOfSlot, &NrOfChList, &ModelList, &DescriptionList, &SerNumList, &FmwRelMinList, &FmwRelMaxList);

      if (retval != CAENHV_OK) {
         print_hv_error("Error getting crate map", retval);
         return retval;
      }

      num_slots = NrOfSlot;
      modules.clear();

      char* m = ModelList;
      char* d = DescriptionList;

      for (unsigned int slot = 0; slot < NrOfSlot; slot++) {
         MODULE mod;
         mod.slot = slot;

         if (*m == 0) {
            mod.present = false;
         } else {
            mod.present = true;
            mod.num_channels = NrOfChList[slot];
            mod.model = std::string(m);
            mod.description = std::string(d);
            mod.serial_number = SerNumList[slot];
            mod.firmware_version = FmwRelMaxList[slot] + (FmwRelMinList[slot] / 10.);

            retval = mod.init_board_parameters(handle);

            if (retval != CAENHV_OK) {
               print_hv_error("Error getting board params", retval);
               return retval;
            }

            retval = mod.init_channel_parameters(handle);

            if (retval != CAENHV_OK) {
               print_hv_error("Error getting channel params", retval);
               return retval;
            }
         }

         m += strlen(m) + 1;
         d += strlen(d) + 1;

         modules.push_back(mod);
      }

      free(NrOfChList);
      free(SerNumList);
      free(ModelList);
      free(DescriptionList);
      free(FmwRelMinList);
      free(FmwRelMaxList);

      return retval;
   }

   CAENHVRESULT CRATE::read_all_param_values() {
      CAENHVRESULT retval = CAENHV_OK;

      for (unsigned int m = 0; m < modules.size(); m++) {
         MODULE& mod = modules[m];

         for (unsigned int p = 0; p < mod.board_params.size(); p++) {
            ParamBase* par = mod.board_params[p];

            if (par->is_readable()) {
               retval = par->read(1);

               if (retval != CAENHV_OK) { 
                  printf("Failed to read board param %s from slot %d\n", par->name.c_str(), mod.slot);
               }
            }
         }

         for (unsigned int p = 0; p < mod.channel_params.size(); p++) {
            ParamBase* par = mod.channel_params[p];

            if (par->is_readable()) {
               retval = par->read(mod.num_channels);

               if (retval != CAENHV_OK) { 
                  printf("Failed to read chan param %s from slot %d\n", par->name.c_str(), mod.slot);
               }
            }
         }
      }

      return retval;
   }

   MODULE& CRATE::get_module(int slot) {
      for (auto it = modules.begin(); it != modules.end(); it++) {
         MODULE& mod = *it;

         if (mod.slot == slot) {
            return mod;
         }
      }

      throw std::out_of_range("No module for that slot number");
   }

} // namespace
