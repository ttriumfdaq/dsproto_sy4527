#ifndef CAENHV_FE_CLASS_H
#define CAENHV_FE_CLASS_H

#include "caenhvwrapper_cxx.h"
#include "midas.h"
#include <vector>
#include <string>
#include <map>
#include <set>

// Struct that matches ODB structure for /Equipment/XYZ/Settings/Global.
typedef struct {
  char hostname[128];
  char username[32];
  char password[32];
  BOOL readonly;
  double check_timeout_secs;
} GlobalSettings;

// Sting to create /Equipment/XYZ/Settings/Global.
#define GLOBAL_SETTINGS_STR "\
Hostname = STRING : [128] \n\
Username = STRING : [32] admin\n\
Password = STRING : [32] admin\n\
Read only = BOOL : y\n\
Timeout to check changes (s) = DOUBLE : 0.5\n\
"

// Struct for caching info about the ODB entry we created for a parameter.
typedef struct {
  std::string odb_name;
  INT odb_type;
  INT odb_num_values;
} ParamOdbInfo;

/*
 * General logic:
 * - Connect to crate, see which modules are present, which parameters
 *   they support and current values.
 * - Create appropriate ODB structure for read-only and read-write settings.
 * - Populate ODB with current values.
 * - Open ODB hotlink for read-write settings, which just sets a flag noting
 *   that we have changes pending (so we do changes in a big batch rather than
 *   one-by-one).
 * - Periodically:
 *   - If changes pending, compare ODB values to in-memory values and write
 *     any changed values to crate.
 *   - Read all values from crate and update ODB with any that have changed.
 *   - There is an edge case where a parameter may be updated in ODB and via
 *     another interface (e.g. ssh/GUI) at the same time. However, that is an
 *     odd case and not worth worrying about.
 *
 * ODB structure:
 *
 * /Equipment
 *   /<fe_name>
 *     /Variables - what's being logged
 *     /Settings
 *       /Global - connection settings
 *       /Slot 1 - RW variables
 *     /Status
 *       /Slot 1 - R variables and meta info (FW version etc)
 *
 */

class CaenHVFE {
   public:
      CaenHVFE();
      ~CaenHVFE();

      // Specify that the ODB shouldn't mention this HV parameter.
      void set_ignore_param(std::string param_name);

      // Specify that any events written to buffers should contain this
      // HV parameter. The actual bank names will contain the slot number,
      // so a bank_prefix of "HV" will result in banks HV01, HV02 etc.
      // param_name is exact name of a parameter from HV module (case-sensitive).
      // bank_prefix must be length 2.
      void set_log_param(std::string param_name, std::string bank_prefix);

      // Provide a more human-friendly name for a parameter. E.g. you could say
      // that "V0Mon" appears as "Measured voltage" in the ODB. If the parameter
      // has any units, they will still be automatically appended to the ODB
      // key name (e.g. "Measured voltage (V)").
      void set_odb_alias(std::string param_name, std::string odb_name);

      // Specify that a different ODB type be used for the given parameter.
      // Most useful if you want on/off parameters to be ints rather than bools.
      // You'll probably have to implement the logic for any type conversions
      // in sync_change(), and possibly add_to_event().
      void set_odb_type(std::string param_name, INT midas_type);

      // Set how often we should sync the ODB/crate values, at most.
      void set_sync_period_ms(float ms);

      // Main function to sync the ODB/crate values.
      // First writes any ODB changes to the crate, then any crate changes
      // to the ODB.
      INT sync();

      // Create a midas event with banks, as specified by user's calls to
      // set_log_param(). Returns the size of the event in bytes.
      INT record_state(char* pevent, INT off);

      // Connect to the crate and initialize ODB based on which modules
      // are present and the parameters they support.
      // _eqp_name is needed so we know where to store our settings/status.
      // _hDB is the database handle.
      // _sys_type is the type of crate we're connecting to.
      INT frontend_init(std::string _eqp_name, HNDLE _hDB, CAENHV_SYSTEM_TYPE_t _sys_type);

      // Any cleanup actions needed.
      INT frontend_exit();

      // Callback function called when global ODB settings change (e.g. crate hostname).
      void odb_global_settings_changed_callback();

      // Callback function called when slot ODB settings change (e.g. voltages).
      void odb_slot_settings_changed_callback();

   private:
      // Connect to the crate, determine which modules are present, and read
      // values for all parameters.
      INT init_crate();

      // Create ODB structure for /Equipment/XYZ/Settings/Global.
      INT create_global_records();

      // Open hotlink to /Equipment/XYZ/Settings/Global.
      INT open_global_records();

      // Create ODB structures for /Equipment/XYZ/[Settings|Status]/Slot N.
      INT create_slot_records();

      // Open hotlinks to /Equipment/XYZ/[Settings|Status]/Slot N.
      INT open_slot_records();

      // Create strings we'll pass to db_check_record() to ensure we have
      // the correct ODB structure for the given slot.
      // Returns pair of strings for /Settings and /Status, respectively.
      std::pair<std::string, std::string> create_odb_slot_strings(int slot);

      // Get the bank name we should use for the data from this parameter and
      // this slot. E.g. if user called set_log_param() to set a "bank prefix"
      // of "HV" for this parameter, and this is slot 4, the bank name is HV04.
      std::string get_bank_name(std::string param_name, int slot);

      // Populate midas event. Returns SUCCESS etc, and fills the size of the
      // event in bytes in event_size.
      INT create_event(char* pevent, INT& event_size);

      // Write the state of the crate to ODB. Only touches variables that have changed.
      INT write_state_to_odb();

      // Write the state of the ODB to the crate. Only touches variables that have changed.
      INT write_odb_changes_to_crate();

      // Either write ODB to crate, or crate to ODB. The actual implementation of
      // write_state_to_odb() and write_odb_changes_to_crate().
      INT sync_odb_crate_changes(bool to_odb);

      // Wait for any changes sent by write_odb_changes_to_crate() to be reflected by
      // the modules. It sometimes takes > 100ms between when we change V0Set (and the
      // library saying the change was successful) and when READING V0Set actually gives
      // us the new value (rather than the old one).
      INT wait_for_changes_to_be_reflected_by_crate();

      // Helper function to create and populate a bank in an event with the
      // data from a parameter.
      INT add_to_event(char* pevent, CAENHV::ParamFloatBase* param, ParamOdbInfo& odb_info, std::string bankname);
      INT add_to_event(char* pevent, CAENHV::ParamUnsignedBase* param, ParamOdbInfo& odb_info, std::string bankname);

      // Helper functions to write parameters to ODB, or write ODB values to the crate.
      INT sync_change(CAENHV::ParamFloatBase* param, ParamOdbInfo& odb_info, int slot, bool to_odb);
      INT sync_change(CAENHV::ParamUnsignedBase* param, ParamOdbInfo& odb_info, int slot, bool to_odb);
      INT sync_change(CAENHV::ParamChanName* param, ParamOdbInfo& odb_info, int slot, bool to_odb);

      // Helper functions to check whether the value we wrote from ODB is now reported back by the crate.
      // Updates the `change_was_recently_sent` flag of `param`.
      void check_change(CAENHV::ParamFloatBase* param, ParamOdbInfo& odb_info, int slot);
      void check_change(CAENHV::ParamUnsignedBase* param, ParamOdbInfo& odb_info, int slot);
      void check_change(CAENHV::ParamChanName* param, ParamOdbInfo& odb_info, int slot);

      // Helper function to see whether ODB values are different to crate values.
      bool has_new_value(float* new_val, CAENHV::ParamFloatBase* param, int num_channels);
      bool has_new_value(unsigned* new_val, CAENHV::ParamUnsignedBase* param, int num_channels);
      bool has_new_value(std::vector<std::string>& new_val, CAENHV::ParamChanName* param, int num_channels);

      // Struct for /Equipment/XYZ/Settings/Global.
      GlobalSettings settings;

      // Previous value of /Equipment/XYZ/Settings/Global, so we can see what
      // the user changed when our hotlink is called.
      GlobalSettings prev_settings;

      // The main CRATE object that provides a objectified interface to the HV
      // crate.
      CAENHV::CRATE crate;

      // How frequently we should read updated from the crate.
      float sync_period_ms;

      // When we last read updated from the crate.
      timeval last_sync_time;

      // Whether the user has made changes in the ODB.
      bool changes_pending;

      // The equipment name we're running as.
      std::string eqp_name;

      // The type of crate we're connecting to.
      CAENHV_SYSTEM_TYPE_t sys_type;

      // Any parameters the user wants us to ignore and not show in the ODB.
      std::set<std::string> ignore_params;

      // Any parameters the user wants logging to buffers in midas events.
      // param name -> bank name prefix.
      std::map<std::string, std::string> log_params;

      // Any human-friendly names to use in ODB.
      // param name -> ODB name.
      std::map<std::string, std::string> odb_aliases;

      // Any ODB types we should use that aren't the defaults.
      // param name -> TID_xyz
      std::map<std::string, int> odb_types;

      // Handles to slot settings (for RW params).
      // slot number -> handle to /Equipment/XYZ/Settings/Slot N
      std::map<int, HNDLE> hSlotSettings;

      // Handles to slot settings (for R params).
      // slot number -> handle to /Equipment/XYZ/Status/Slot N
      std::map<int, HNDLE> hSlotStatus;

      // ODB info about each RW parameter. Filled when we set up the ODB in
      // frontend_init(), and recorded here so we don't have to look it
      // up again later.
      // slot number -> parameter -> ODB info.
      std::map<int, std::map<CAENHV::ParamBase*, ParamOdbInfo> > odb_info_slot_settings;

      // ODB info about each R parameter. Filled when we set up the ODB in
      // frontend_init(), and recorded here so we don't have to look it
      // up again later.
      // slot number -> parameter -> ODB info.
      std::map<int, std::map<CAENHV::ParamBase*, ParamOdbInfo> > odb_info_slot_status;

      // Main database handle.
      HNDLE hDB;

      // Handle to /Equipment/XYZ.
      HNDLE hBase;

      // Handle to /Equipment/XYZ/Settings.
      HNDLE hSettings;

      // Handle to /Equipment/XYZ/Settings/Global.
      HNDLE hGlobalSettings;

      // Handle to /Equipment/XYZ/Status.
      HNDLE hStatus;

      std::map<int, std::map<CAENHV::ParamBase*, std::vector<float> > > cached_float_odb_change_sent;
      std::map<int, std::map<CAENHV::ParamBase*, std::vector<unsigned> > > cached_unsigned_odb_change_sent;
      std::map<int, std::map<CAENHV::ParamBase*, std::vector<std::string> > > cached_string_odb_change_sent;
};

#endif
