#ifndef CAENHVWRAPPER_CXX_H
#define CAENHVWRAPPER_CXX_H

#include "CAENHVWrapper.h"
#include <vector>
#include <string>

/*
 * This code provides an object-oriented wrapper around the official CAENHVWrapper
 * library (which uses some tricky/tedious C).
 *
 * The main CRATE object will connect to a specific HV crate, determine which
 * modules are present, which parameters each module supports, and give an
 * objectified representation of the state.
 *
 * It supports almost all the parameter types that can be present on CAEN HV
 * modules, though STRING support is not fully implemented.
 *
 * Unlike the official C library, we treat the channel name as if it were a
 * regular parameter, rather than having to call a specific set of functions
 * just for that.
 */
namespace CAENHV {

   // Parameter types we support. Values come from #defines in the C library.
   enum ParamType {
     Numeric = PARAM_TYPE_NUMERIC,
     OnOff = PARAM_TYPE_ONOFF,
     ChanStatus = PARAM_TYPE_CHSTATUS,
     BoardStatus = PARAM_TYPE_BDSTATUS,
     Binary = PARAM_TYPE_BINARY,
     String = PARAM_TYPE_STRING,
     Enum = PARAM_TYPE_ENUM,
     ChanName = 12345 // Custom
   };

   // Parameter modes (R/W/RW).
   enum ParamMode {
      RdOnly = PARAM_MODE_RDONLY,
      WrOnly = PARAM_MODE_WRONLY,
      RdWr = PARAM_MODE_RDWR
   };

   // Helper functions for converting various enums/status codes
   // to a textual representation of their meaning.
   std::string param_type_to_text(ParamType code);
   std::string param_mode_to_text(ParamMode code);
   std::string error_code_to_text(int code);
   std::string chan_status_bit_to_text(int code);
   std::string board_status_bit_to_text(int code);

   // Print an error to screen. Prints the user's message, then the integer code,
   // then the textual meaning of that code.
   void print_hv_error(std::string preamble, int code);

   // Base class for all parameters.
   // All derived class should implement:
   // - a way to store the appropriate value type (string/int/float etc).
   //   If a board parameter, there is only one value; if a channel parameter
   //   there is one element per channel.
   // - the pure-virtual functions read()/write()/value_to_text().
   class ParamBase {
      public:
         ParamBase(bool _is_board_param, int _handle, int _slot, char* param_name, ParamType param_type);
         virtual ~ParamBase() {};

         // Whether this parameter is writable on the crate, or just read-only.
         bool is_writable();

         // Whether this parameter is readable on the create, or just write-only.
         bool is_readable();

         // Textual description of this parameter.
         virtual std::string description();

         // Read the current value of this parameter from the crate, and store
         // it somewhere in the derived class.
         virtual CAENHVRESULT read(int num_channels = 1) = 0;

         // Write the new values to the crate, but do NOT update the value
         // stored in the derived class (we'll call read() again later).
         virtual CAENHVRESULT write(void* new_values, int num_channels = 1) = 0;

         // Convert the value stored in the derived class to text.
         // If "fancy" is false, the value should be in a basic format that
         // can be used to populate the ODB in a check_record string; if it's
         // true you can provide a more human-friendly value.
         virtual std::string value_to_text(int chan_idx, bool fancy=false) = 0;

         // Parameter name as reported by the crate.
         std::string name;

         // Type of the parameter (allows us to cast to the correct
         // derived class later).
         ParamType type;

         // Read/write mode of this parameter.
         ParamMode mode;

         // Whether this is a per-channel or per-board parameter.
         bool is_board_param;

         // Crate connection handle.
         int handle;

         // Slot number.
         int slot;

         // Whether we recently sent a new value of this param to the module.
         bool change_was_recently_sent;
   };

   // More refined base class for any parameters that use float values.
   class ParamFloatBase : public ParamBase {
      public:
         ParamFloatBase(bool is_board_param, int handle, int slot, char* param_name, ParamType param_type);

         virtual ~ParamFloatBase();

         CAENHVRESULT read(int num_channels = 1);
         CAENHVRESULT write(void* new_values, int num_channels = 1);
         virtual std::string value_to_text(int chan_idx, bool fancy=false);

         float* value;
   };

   // More refined base class for any parameters that use unsigned int values.
   class ParamUnsignedBase : public ParamBase {
      public:
         ParamUnsignedBase(bool is_board_param, int handle, int slot, char* param_name, ParamType param_type);

         virtual ~ParamUnsignedBase();

         CAENHVRESULT read(int num_channels = 1);
         CAENHVRESULT write(void* new_values, int num_channels = 1);
         virtual std::string value_to_text(int chan_idx, bool fancy=false);

         unsigned* value;
   };

   // Specific class for setting/reading channel names.
   class ParamChanName : public ParamBase {
      public:
         ParamChanName(int handle, int slot, char* par_name);

         CAENHVRESULT read(int num_channels = 1);
         CAENHVRESULT write(void* new_values, int num_channels = 1);
         virtual std::string value_to_text(int chan_idx, bool fancy=false);

         std::vector<std::string> value;
   };

   // Specific class for setting/reading float values and their units.
   class ParamNumeric : public ParamFloatBase {
      public:
         ParamNumeric(bool is_board_param, int handle, int slot, char* param_name);
         virtual std::string description();

         float min_val;
         float max_val;
         std::string units;
   };

   // Specific class for setting/reading boolean parameters.
   class ParamOnOff : public ParamUnsignedBase {
      public:
         ParamOnOff(bool is_board_param, int handle, int slot, char* param_name);
         virtual std::string value_to_text(int chan_idx, bool fancy=false);

         // Crate can report what true/false really mean (e.g. "Enabled"/"Disabled",
         // or "Ramp"/"Kill").
         char onstate[30];
         char offstate[30];
   };

   // Specific class for setting/reading channel status.
   class ParamChanStatus : public ParamUnsignedBase {
      public:
         ParamChanStatus(int handle, int slot, char* param_name);

         // Get a string containing which bits are set.
         std::string explain_bits(int chan_idx);

         enum Bits {
            On,
            RampingUp,
            RampingDown,
            OverCurrent,
            OverVoltage,
            UnderVoltage,
            ExternalTrip,
            MaxV,
            ExternalDisable,
            InternalTrip,
            CalibrationError,
            Unplugged,
            MaxChanParamBit
         };
   };

   // Specific class for setting/reading board status.
   class ParamBoardStatus : public ParamUnsignedBase {
      public:
         ParamBoardStatus(int handle, int slot, char* param_name);

         // Get a string containing which bits are set.
         std::string explain_bits();

         enum Bits {
            PowerFail,
            FirmwareChecksumError,
            CalibrationErrorHV,
            CalibrationErrorTemp,
            UnderTemperature,
            OverTemperature,
            MaxBoardParamBit
         };
   };

   // Specific class for setting/reading binary parameters.
   class ParamBinary : public ParamUnsignedBase {
      public:
         ParamBinary(bool is_board_param, int handle, int slot, char* param_name);
   };

   // Specific class for setting/reading enum parameters.
   class ParamEnum : public ParamUnsignedBase {
      public:
         ParamEnum(bool is_board_param, int handle, int slot, char* param_name);
         virtual std::string value_to_text(int chan_idx, bool fancy=false);

         int minval;
         std::vector<std::string> meanings;
   };

   // Class containing metadata and parameters for a single slot/module in a crate.
   class MODULE {
      public:
         // See which board parameters the module supports, and set up
         // board_params. slot must already be set.
         CAENHVRESULT init_board_parameters(int handle);

         // See which per-channel parameters the module supports, and set up
         // channel_params. slot must already be set.
         CAENHVRESULT init_channel_parameters(int handle);

         // Whether this module supports the given per-channel parameter.
         bool has_channel_param(std::string name);

         // Get the specified per-channel parameter. Returns NULL if
         // parameter doesn't exist.
         ParamBase* get_channel_param(std::string name);

         // Helper function to delete that ParamBase objects created with "new".
         void free_params();

         // Which slot this MODULE object represents.
         int slot;

         // Whether this module is physically present in the crate.
         bool present;

         // How many channels this module powers.
         int num_channels;

         // The model name of this module.
         std::string model;

         // The description of this module, as reported by the crate.
         std::string description;

         // The serial number of this module.
         int serial_number;

         // The firmware version of this module.
         float firmware_version;

         // Board parameters we discovered.
         std::vector<ParamBase*> board_params;

         // Per-channel parameters we discovered.
         std::vector<ParamBase*> channel_params;
   };

   // Class representing a CAEN HV crate and all the modules in it.
   class CRATE {
      public:
         // Basic constructor. Doesn't do any real work.
         CRATE();

         // Connect to the specified crate and see what modules are present.
         CAENHVRESULT init(CAENHV_SYSTEM_TYPE_t sys_type, std::string ip_addr, std::string username, std::string password);

         // Read the values of all parameters of all modules.
         CAENHVRESULT read_all_param_values();

         // Get a specific module. Throws std::out_of_range() if an invalid slot
         // is specified.
         MODULE& get_module(int slot);

         // Crate connection handle returned by C library.
         int handle;

         // Number of slots this crate has.
         int num_slots;

         // The modules in the slots of this crate.
         std::vector<MODULE> modules;
   };

} // namespace
#endif
