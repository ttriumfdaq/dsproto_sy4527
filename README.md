# Documentation for the DS-Proto SY4527 HV control

This repository contains a midas frontend for CAEN HV crates. It was originally written for an SY4527, but should work with most CAEN HV systems that have an ethernet interface (e.g. SY5527, SY1527, SY2527, N1470 etc...)

It contains:

* A "frontend" C++ program that automatically detects the modules present in the crate, and allows the user to configure voltages etc in the midas ODB. Changes are automatically synced between the ODB and crate (in both directions).
* A command-line tool for dumping the state of a crate as text.
* A webpage to simplify editing/displaying the ODB parameters.
* A python tool for generating IV curves - set the voltage of a channel, read the current.

There is a related packages (dsproto_steering) that can be used for interacting with the Darkside-specific Steering Module and Control Module boards, which "fan out" an HV/LV signal from the crate to multiple SiPMs.

## Install prerequisites

* Install midas (2020-12 or later recommended).
* Install the CAENHVWrapper from CAEN - see https://www.caen.it/products/caen-hv-wrapper-library/ . Download the .tgz file and extract it. No need to compile anything or run any installation scripts.

## Install this packages

First, figure out the arguments you need to pass to `cmake` when compiling this code. Note the directory structure changed between releases 5.8 and 6.0!

* `CAEN_INC_DIR` will point to the `include` directory within the CAEN HV code. E.g. `-DCAEN_INC_DIR=$HOME/packages/CAENHVWrapper-5.82/include` or `-DCAEN_INC_DIR=$HOME/packages/CAENHVWrapper-6.3/include`
* `CAEN_LIB` to point to the `.so` file within the CAEN HV code. E.g. `-DCAEN_LIB=$HOME/packages/CAENHVWrapper-5.82/lib/libcaenhvwrapper.so.5.82` or `-DCAEN_LIB=$HOME/packages/CAENHVWrapper-6.3/bin/x86_64/libcaenhvwrapper.so.6.3`.
* By default the executables will be installed in `/path/to/dsproto_sy4527/bin`. If you want them installing somewhere else, use `-DINSTALL_DIR=/path/to/install/dir`.

Now we can actually do the installation:

* Download this repository `git clone https://bitbucket.org/ttriumfdaq/dsproto_sy4527.git`
* `cd dsproto_sy4527`
* `mkdir build`
* `cd build`
* Run cmake with all the definitions noted above. For example: `cmake .. -DCAEN_INC_DIR=~/packages/CAENHVWrapper-6.3/include -DCAEN_LIB=~/packages/CAENHVWrapper-6.3/bin/x86_64/libcaenhvwrapper.so.6.3`
* `make install`

## Configure the ODB

* Edit the ODB to add a custom webpage: create a key `/Custom/CAEN HV` and set it to `/path/to/dsproto_sy4527/caenhv.html`. A link to the page should appear in the menu on the left of all midas webpages.
* Run `bin/caenhv_fe` once to setup the ODB structure.
* Set the hostname of your crate in the ODB at `/Equipment/CAEN_HV/Settings/Global/Hostname`.
* If using non-default values, also set the Username and Password in `/Equipment/CAEN_HV/Settings/Global`.
* Run `bin/caenhv_fe` to verify that the code can talk to the crate. Note that for safety, we default to starting in "read-only" mode. The "CAEN HV" webpage should show you the modules that are installed in your crate.
* Once you are happy that the webpage values match reality, you can disable "read-only" mode. Set `/Equipment/CAEN_HV/Settings/Global/Read only` to `n`.

## Full list of executables

* `caenhv_fe` - main frontend for controlling an SY4527 via midas.
* `caenhv_print_state` - dump the state of an SY4527 to screen. No midas integration. 
* `iv_curve.py` - script to automate controlling the SY4527 (and optionally the Steering Module) to produce IV curves. Current is read from the SY4527. Can be used with or without a Darkside steering/control module. If using a steering/control module, then you also need the dsproto_steering repository.
* `sy4527` - basic, old-school, not-very-user-friendly version for controlling an SY4527 based on the midas slow-control interface. Not recommended.

