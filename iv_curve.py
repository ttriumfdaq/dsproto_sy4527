#!/usr/bin/python3
"""
Script to control the steering module and Caen/Keithley HV to produce IV curves for
each channel in Darkside Proto-0. Currently we just produce CSV files.

When using Caen HV, we only use 1 channel (which is connected to the steering module), 
then scan through the steering module channels at each voltage.

The Keithley SMU only has one channel anyway.

Run with the -h argument to get usage instructions.

Frontends that must be running:

* If using CAEN HV: CAEN_HV frontend (caenhv_fe in dsproto_sy4527)
* If using Keithley HV: Keithley2450SMU frontend (fe_keithley.py in dsproto_hvlv)
* If using Wiener HV: fewiener frontend (fewiener.py in dsproto_wiener)
* If using steering module: steering module frontend (midas/steering_module_frontend.py in dsproto_steering)
* If using control module: control module frontend (midas/control_module_frontend.py in dsproto_steering)
"""

import midas
import midas.client
import argparse
import datetime
import random
import time
import sys

class IVCurve():
    """
    Class to control the IV curve acquisition.
    
    Members:
        * chan_list (list of int) - The HV steering module channels to control.
        * chosen_voltages (list of float) - The HV voltage to run at.
        * odb_demand_path (str) - How to set the HV voltage
        * odb_measured_path (str) - How to read the HV voltage
        * odb_current_path (str) - How to read the HV current
        * odb_ramp_up_path (str) - How to read the HV ramp up rate
        * odb_ramp_down_path (str) - How to read the HV ramp down rate
        * odb_switch_path (str) - How to force a channel on/off (only needed for Wiener)
        * hv_readback_tolerance (float) - How close the actual voltage must
            be to the set voltage before we'll start the current measurement
        * hv_stability_tolerance (float) - How stable the current reading
            must be before we accept it as valid
        * settling_time_ms (float) - How long to wait after setting the voltage
            or channel before starting to read the current
        * dummy_caen (bool) - Whether to fake talking to the CAEN HV
        * enable_steering (bool) - Whether to talk to the steering module
        * enable_control (bool) - Whether to talk to the control module
        * client (`midas.client.MidasClient`)
    """
    def __init__(self):
        self.chan_list = [None]

        self.chosen_voltages = []
        self.odb_demand_path = None
        self.odb_measured_path = None
        self.odb_current_path = None
        self.odb_ramp_up_path = None
        self.odb_ramp_down_path = None
        self.odb_switch_path = None
        self.odb_trip_path = None
        self.odb_limit_path = None
        self.dummy_caen = False
        self.dummy_caen_v = 0
        self.dummy_caen_fake_failure = {}
        self.hv_readback_tolerance = None
        self.hv_stability_tolerance = None
        self.hv_current_limit = None
        self.hv_trip_time = None
        self.settling_time_ms = None
        self.enable_steering = None
        self.enable_control = None
        
        self.client = None

        self.sm_client_name = "SteeringModule"
        self.sm_equip_base = "/Equipment/SteeringModule"
        self.sm_uses_pduplus = False

        self.cm_client_name = "ControlModule"
        self.cm_equip_base = "/Equipment/ControlModule"
        self.cm_pdu_list = []
        
    def parse_args(self):
        """
        Parse command-line arguments, and set up class member variables.
        """
        desc = "Scan through voltages and steering module channels to produce IV curves."
        parser = argparse.ArgumentParser(description=desc)
        
        v_args = parser.add_argument_group("Voltage settings")
        v_args.add_argument("--custom_v", type=str, help="Set different increments for different voltage regions. Format is like '0:50:2,50:60:1,60:80:0.5' to scan in 2V increments between 0V and 50V then 1V increments between 50V and 60V etc.")
        v_args.add_argument("--start_v", type=float, default=0, help="Start voltage (V), default is 0")
        v_args.add_argument("--stop_v", type=float, default=80, help="Stop voltage (V), default is 80")
        v_args.add_argument("--step_v", type=float, default=0.2, help="Increment between scan points (V), default is 0.2")
        v_args.add_argument("--settling_time_ms", type=int, default=500, metavar="DELAY_ms", help="How long to wait after changing the voltage/channel before reading current (ms), default is 500")
        v_args.add_argument("--abort_on_high_hv_current", action="store_true", help="Abort program if current gets too high (default is to just move to the next tile)")
        
        caen_args = parser.add_argument_group("CAEN SY HV settings")
        caen_args.add_argument("--caen_slot", type=int, default=4, help="Which Caen HV slot is connected to the steering module, default is 4")
        caen_args.add_argument("--caen_chan", type=int, default=0, help="Which Caen HV channel is connected to the steering/control module, default is 0")
        caen_args.add_argument("--caen_chan_list", help="Comma-separated list of Caen HV channels that are connected to the control module, if running multiple PDUs in parallel")
        caen_args.add_argument("--caen_readback_tolerance", type=float, default=0.11, metavar="TOLERANCE", help="Tolerance for ensuring the readback Caen HV matches the set voltage (in whatever units the module uses), default is 0.11")
        caen_args.add_argument("--caen_stability_tolerance", type=float, default=0.1, metavar="STABILITY", help="Tolerance for ensuring the current read by Caen HV is stable (in whatever units the module uses), default is 0.1")
        caen_args.add_argument("--caen_current_limit", type=float, default=100, metavar="LIMIT", help="Current limit to apply to the CAEN HV channel being controlled (in whatever units the module uses), default is 100")
        caen_args.add_argument("--dummy_caen", action="store_true", help="Fake talking to the CAEN HV module")
        
        k_args = parser.add_argument_group("Keithley HV settings")
        k_args.add_argument("--use_keithley", action="store_true", help="Use Keithley 2450 SMU rather than CAEN SY for HV. Default is to use CAEN.")
        k_args.add_argument("--keithley_readback_tolerance", type=float, default=0.01, metavar="TOLERANCE", help="Tolerance for ensuring the voltage readback Keithley SMU matches the set voltage (in Volts), default is 0.01")
        k_args.add_argument("--keithley_stability_tolerance", type=float, default=0.1, metavar="STABILITY", help="Tolerance for ensuring the current read by Keithley SMU is stable (in mA), default is 0.1")
        k_args.add_argument("--keithley_current_limit", type=float, default=1, metavar="LIMIT", help="Current limit to apply to the Keithley SMU (in mA), default is 1")

        w_args = parser.add_argument_group("Wiener HV settings")
        w_args.add_argument("--use_wiener", action="store_true", help="Use Wiener rather than CAEN for HV. Default is to use CAEN.")
        w_args.add_argument("--wiener_slot", type=int, default=0, help="Which Wiener slot is connected to the steering module, default is 0")
        w_args.add_argument("--wiener_chan", type=int, default=0, help="Which Wiener channel is connected to the steering module, default is 0")
        w_args.add_argument("--wiener_chan_list", help="Comma-separated list of Wiener channels that are connected to the control module, if running multiple PDUs in parallel")
        w_args.add_argument("--wiener_readback_tolerance", type=float, default=0.01, metavar="TOLERANCE", help="Tolerance for ensuring the voltage readback by Wiener matches the set voltage (in Volts), default is 0.01")
        w_args.add_argument("--wiener_stability_tolerance", type=float, default=0.0001, metavar="STABILITY", help="Tolerance for ensuring the current read by Wiener is stable (in A), default is 0.0001 (0.1mA)")
        w_args.add_argument("--wiener_current_limit", type=float, default=0.001, metavar="LIMIT", help="Current limit to apply to the Wiener (in A), default is 0.001 (1mA)")

        o_args = parser.add_argument_group("Output settings")
        o_args.add_argument("--filename", type=str, metavar="OUTPUT_FILE", help="Save CSV data in output file (default is to just print to screen)")
        
        s_args = parser.add_argument_group("Steering module settings")
        s_args.add_argument("--no_steering", action="store_true", help="Don't talk to the steering module")
        s_args.add_argument("--sm_suffix", default="", help="E.g. if steering module's ODB settings are in /Equipment/SteeringModulePDU5, specify PDU5")
        s_args.add_argument("--exclude_pdu_sm_chans", type=str, metavar="EXCLUDE_LIST", help="Comma-separated list of PDU steering module HV channels to skip (default is scan 0-24 and exclude nothing)")
        s_args.add_argument("--only_pdu_sm_chans", type=str, metavar="ONLY_LIST", help="Comma-separated list of PDU steering module HV channels to scan (default is scan 0-24)")
        s_args.add_argument("--exclude_vpdu_sm_tiles", type=str, metavar="EXCLUDE_LIST", help="Comma-separated list of vPDU steering module HV channels to skip (default is to scan tiles 1-4 in quadrants 1-4 and exclude nothing). Specify as quad.tile. E.g. 1.1,1.2,1.3")
        s_args.add_argument("--only_vpdu_sm_tiles", type=str, metavar="ONLY_LIST", help="Comma-separated list of vPDU steering module HV channels to scan (default is to scan tiles 1-4 in quadrants 1-4). Specify as quad.tile. E.g. 1.1,1.2,1.3")
        
        c_args = parser.add_argument_group("Control module settings")
        c_args.add_argument("--use_control_module", action="store_true", help="Use the control module rather than the steering module")
        c_args.add_argument("--cm_suffix", default="", help="E.g. if control module's ODB settings are in /Equipment/ControlModule2, specify 2")
        c_args.add_argument("--cm_pdu_list", type=str, metavar="PDU_LIST", help="Comma-separated list of PDUs to scan through (0-15). Default is all PDUs that are currently set to 'Enabled' in the ODB. Any PDUs specified in this list will be Enabled automatically.")
        c_args.add_argument("--cm_exclude_vpdu_tiles", type=str, metavar="EXCLUDE_LIST", help="Comma-separated list of vPDU HV channels to skip when using the control module (default is to scan tiles 1-4 in quadrants 1-4 and exclude nothing). Applies to all PDUs in --cm_pdu_list. Specify as quad.tile. E.g. 1.1,1.2,1.3")
        c_args.add_argument("--cm_only_vpdu_tiles", type=str, metavar="ONLY_LIST", help="Comma-separated list of vPDU HV channels to scan when using the control module (default is to scan tiles 1-4 in quadrants 1-4). Applies to all PDUs in --cm_pdu_list. Specify as quad.tile. E.g. 1.1,1.2,1.3")

        args = parser.parse_args()
        
        self.client = midas.client.MidasClient("IVcurve")
        
        self.enable_control = args.use_control_module
        self.enable_steering = not self.enable_control and not args.no_steering
        self.abort_on_high_hv_current = args.abort_on_high_hv_current
        
        if self.enable_control:
            self.cm_client_name = "ControlModule%s" % args.cm_suffix
            self.cm_equip_base = "/Equipment/ControlModule%s" % args.cm_suffix
            cm_settings = self.client.odb_get("%s/Settings" % self.cm_equip_base)

            if args.cm_exclude_vpdu_tiles and args.cm_only_vpdu_tiles:
                raise ValueError("Specify only --cm_exclude_vpdu_tiles or --cm_only_vpdu_tiles")

            if args.exclude_pdu_sm_chans or args.only_pdu_sm_chans or args.exclude_vpdu_sm_tiles or args.only_vpdu_sm_tiles:
                raise ValueError("--control_module is not compatible with --exclude_pdu_sm_chans, --only_pdu_sm_chans, --exclude_vpdu_sm_tiles, --only_vpdu_sm_tiles")

            if args.cm_pdu_list:
                self.cm_pdu_list = [int(x) for x in args.cm_pdu_list.split(",")]

                for pdu in self.cm_pdu_list:
                    if not cm_settings["PDU %d" % pdu]["Enable"]:
                        raise ValueError("PDU %d was specified to be scanned, but is disabled in ODB - either enable it or remove from --cm_pdu_list" % pdu)
            else:
                for pdu in range(16):
                    if cm_settings["PDU %d" % pdu]["Enable"]:
                        self.cm_pdu_list.append(pdu)

            for pdu in self.cm_pdu_list:
                if pdu < 0 or pdu > 15:
                    raise ValueError("PDUs are numbered 0-15. PDU %d is invalid." % pdu)
            
            if len(self.cm_pdu_list) == 0:
                if args.cm_pdu_list:
                    raise ValueError("Empty PDU list provided")
                else:
                    raise ValueError("No PDUs are Enabled in the ODB. Either list PDUs manually with --cm_pdu_list (and the script will enable those PDUs for you) or enable some PDUs in the ODB.")

            if args.cm_exclude_vpdu_tiles:
                self.chan_list = ["1.1", "1.2", "1.3", "1.4", "2.1", "2.2", "2.3", "2.4", "3.1", "3.2", "3.3", "3.4", "4.1", "4.2", "4.3", "4.4"]
                
                for quad_tile in args.cm_exclude_vpdu_tiles.split(","):
                    try:
                        (quad, tile) = self.split_quad_tile(quad_tile)
                    except:
                        raise ValueError("Invalid quad/tile number - specify as quad.tile, like 1.3,2.4") 

                    if quad < 1 or quad > 4 or tile < 1 or tile > 4:
                        raise ValueError("Unexpected quad/tile number - ranges are 1-4")

                    self.chan_list.remove(quad_tile)
            elif args.cm_only_vpdu_tiles:
                self.chan_list = []

                for quad_tile in args.cm_only_vpdu_tiles.split(","):
                    try:
                        (quad, tile) = self.split_quad_tile(quad_tile)
                    except:
                        raise ValueError("Invalid quad/tile number - specify as quad.tile, like 1.3,2.4") 

                    if quad < 1 or quad > 4 or tile < 1 or tile > 4:
                        raise ValueError("Unexpected quad/tile number - ranges are 1-4")

                    self.chan_list.append(quad_tile)
            else:
                self.chan_list = ["1.1", "1.2", "1.3", "1.4", "2.1", "2.2", "2.3", "2.4", "3.1", "3.2", "3.3", "3.4", "4.1", "4.2", "4.3", "4.4"]

        if self.enable_steering:
            self.sm_client_name = "SteeringModule%s" % args.sm_suffix
            self.sm_equip_base = "/Equipment/SteeringModule%s" % args.sm_suffix

            if not self.client.odb_exists(self.sm_equip_base):
                valid_suffixes = [k.replace("SteeringModule", "") for k in self.client.odb_get("/Equipment", recurse_dir=False).keys() if k.startswith("SteeringModule")]
                raise ValueError("Failed to find ODB path %s. Did you mean to specify a --sm_suffix from %s?" % valid_suffixes)
            
            self.sm_uses_pduplus = self.client.odb_get("%s/Settings/Use PDUPlus map" % self.sm_equip_base)

            if (not self.sm_uses_pduplus) and (args.exclude_vpdu_sm_tiles or args.only_vpdu_sm_tiles):
                raise ValueError("Steering module uses old PDU map: specify --exclude_pdu_sm_chans/--only_pdu_sm_chans not --exclude_vpdu_sm_tiles/--only_vpdu_sm_tiles")
            if (self.sm_uses_pduplus) and (args.exclude_pdu_sm_chans or args.only_pdu_sm_chans):
                raise ValueError("Steering module uses PDUplus map: specify --exclude_vpdu_sm_tiles/--only_vpdu_sm_tiles not --exclude_pdu_sm_chans/--only_pdu_sm_chans")

            if args.exclude_pdu_sm_chans and args.only_pdu_sm_chans:
                raise ValueError("Specify only --exclude_pdu_sm_chans or --only_pdu_sm_chans")
            if args.exclude_vpdu_sm_tiles and args.only_vpdu_sm_tiles:
                raise ValueError("Specify only --exclude_vpdu_sm_tiles or --only_vpdu_sm_tiles")
            
            if args.cm_exclude_vpdu_tiles or args.cm_only_vpdu_tiles:
                raise ValueError("--cm_exclude_vpdu_tiles and --cm_only_vpdu_tiles are only valid with --control_module")

            if args.exclude_pdu_sm_chans:
                exclude_list = [int(x.strip()) for x in args.exclude_pdu_sm_chans.split(",")]
                
                for x in exclude_list:
                    if x < 0 or x > 24:
                        raise ValueError("Unexpected channel number %s - range is 0-24" % x)
                
                self.chan_list = [c for c in range(25) if c not in exclude_list]
            elif args.only_pdu_sm_chans:
                self.chan_list = [int(x.strip()) for x in args.only_pdu_sm_chans.split(",")]
        
                for x in self.chan_list:
                    if x < 0 or x > 24:
                        raise ValueError("Unexpected channel number %s - range is 0-24" % x)
            else:
                self.chan_list = list(range(25))

            if args.exclude_vpdu_sm_tiles:
                self.chan_list = ["1.1", "1.2", "1.3", "1.4", "2.1", "2.2", "2.3", "2.4", "3.1", "3.2", "3.3", "3.4", "4.1", "4.2", "4.3", "4.4"]
                
                for quad_tile in args.exclude_vpdu_sm_tiles.split(","):
                    try:
                        (quad, tile) = self.split_quad_tile(quad_tile)
                    except:
                        raise ValueError("Invalid quad/tile number - specify as quad.tile, like 1.3,2.4") 

                    if quad < 1 or quad > 4 or tile < 1 or tile > 4:
                        raise ValueError("Unexpected quad/tile number - ranges are 1-4")

                    self.chan_list.remove(quad_tile)
            elif args.only_vpdu_sm_tiles:
                self.chan_list = []

                for quad_tile in args.only_vpdu_sm_tiles.split(","):
                    try:
                        (quad, tile) = self.split_quad_tile(quad_tile)
                    except:
                        raise ValueError("Invalid quad/tile number - specify as quad.tile, like 1.3,2.4") 

                    if quad < 1 or quad > 4 or tile < 1 or tile > 4:
                        raise ValueError("Unexpected quad/tile number - ranges are 1-4")

                    self.chan_list.append(quad_tile)
            else:
                self.chan_list = ["1.1", "1.2", "1.3", "1.4", "2.1", "2.2", "2.3", "2.4", "3.1", "3.2", "3.3", "3.4", "4.1", "4.2", "4.3", "4.4"]

        else:            
            if args.exclude_pdu_sm_chans or args.only_pdu_sm_chans or args.exclude_vpdu_sm_tiles or args.only_vpdu_sm_tiles:
                raise ValueError("Don't specify --exclude_pdu_sm_chans/--only_pdu_sm_chans/--exclude_vpdu_sm_tiles/--only_vpdu_sm_tiles with --no_steering")
        
        self.dummy_caen = args.dummy_caen
        self.use_keithley = args.use_keithley
        self.use_wiener = args.use_wiener

        if self.use_keithley:
            for k in sys.argv:
                if k.find("caen") != -1 or k.find("wiener") != -1:
                    raise ValueError("%s is not a valid option when running with --use_keithley")
                
            self.odb_demand_path = ["/Equipment/Keithley2450SMU/Settings/Set voltage (V)"]
            self.odb_limit_path = ["/Equipment/Keithley2450SMU/Settings/Current limit (mA)"]
            self.odb_ramp_down_path = [None]
            self.odb_ramp_up_path = [None]
            self.odb_trip_path = [None]
            self.odb_switch_path = [None]
            self.odb_measured_path = ["/Equipment/Keithley2450SMU/Readback/Measured voltage (V)"]
            self.odb_current_path = ["/Equipment/Keithley2450SMU/Readback/Measured current (mA)"]

            self.hv_current_limit = args.keithley_current_limit
            self.hv_readback_tolerance = args.keithley_readback_tolerance
            self.hv_stability_tolerance = args.keithley_stability_tolerance
        elif self.use_wiener:
            for k in sys.argv:
                if k.find("caen") != -1 or k.find("keithley") != -1:
                    raise ValueError("%s is not a valid option when running with --use_wiener")
                
            if args.wiener_chan_list is None:
                wiener_chan_list = [args.wiener_chan]
            else:
                wiener_chan_list = [int(x) for x in args.wiener_chan_list.split(",")]

            self.odb_demand_path = [None] * len(wiener_chan_list)
            self.odb_limit_path = [None] * len(wiener_chan_list)
            self.odb_ramp_down_path = [None] * len(wiener_chan_list)
            self.odb_ramp_up_path = [None] * len(wiener_chan_list)
            self.odb_trip_path = [None] * len(wiener_chan_list)
            self.odb_switch_path = [None] * len(wiener_chan_list)
            self.odb_measured_path = [None] * len(wiener_chan_list)
            self.odb_current_path = [None] * len(wiener_chan_list)

            w_odb_base = "/Equipment/Wiener/Settings/Slot %d" % args.wiener_slot

            for idx, w_chan in enumerate(wiener_chan_list):
                self.odb_switch_path[idx] = "%s/Switch[%s]" % (w_odb_base, w_chan)
                self.odb_demand_path[idx] = "%s/Voltage[%s]" % (w_odb_base, w_chan)
                self.odb_limit_path[idx] = "%s/Current limit[%s]" % (w_odb_base, w_chan)

            w_odb_base = "/Equipment/Wiener/Readback/Slot %d" % args.wiener_slot
            
            for idx, caen_chan in enumerate(wiener_chan_list):
                self.odb_measured_path[idx] = "%s/Sense voltage[%s]" % (w_odb_base, w_chan)
                self.odb_current_path[idx] = "%s/Current[%s]" % (w_odb_base, w_chan)

            self.hv_current_limit = args.wiener_current_limit
            self.hv_readback_tolerance = args.wiener_readback_tolerance
            self.hv_stability_tolerance = args.wiener_stability_tolerance
        else:
            for k in sys.argv:
                if k.find("keithley") != -1 or k.find("wiener") != -1:
                    raise ValueError("%s is not a valid option when running in CAEN HV mode")

            if args.caen_chan_list is None:
                caen_chan_list = [args.caen_chan]
            else:
                caen_chan_list = [int(x) for x in args.caen_chan_list.split(",")]

            self.odb_demand_path = [None] * len(caen_chan_list)
            self.odb_limit_path = [None] * len(caen_chan_list)
            self.odb_ramp_down_path = [None] * len(caen_chan_list)
            self.odb_ramp_up_path = [None] * len(caen_chan_list)
            self.odb_trip_path = [None] * len(caen_chan_list)
            self.odb_switch_path = [None] * len(caen_chan_list)
            self.odb_measured_path = [None] * len(caen_chan_list)
            self.odb_current_path = [None] * len(caen_chan_list)

            if not self.dummy_caen:
                # ODB key names can vary based on the units each module uses (e.g. "IMon (uA)" vs "IMon (A)".
                # Find the correct key for the different things we need to interact with.
                caen_odb_base = "/Equipment/CAEN_HV/Settings/Slot %d" % args.caen_slot

                for k in self.client.odb_get(caen_odb_base).keys():
                    for idx, caen_chan in enumerate(caen_chan_list):
                        if k.startswith("V0Set "):
                            self.odb_demand_path[idx] = "%s/%s[%s]" % (caen_odb_base, k, caen_chan)
                        if k.startswith("I0Set "):
                            self.odb_limit_path[idx] = "%s/%s[%s]" % (caen_odb_base, k, caen_chan)
                        if k.startswith("RUp "):
                            self.odb_ramp_up_path[idx] = "%s/%s[%s]" % (caen_odb_base, k, caen_chan)
                        if k.startswith("RDWn "):
                            self.odb_ramp_down_path[idx] = "%s/%s[%s]" % (caen_odb_base, k, caen_chan)
                        if k.startswith("Trip "):
                            self.odb_trip_path[idx] = "%s/%s[%s]" % (caen_odb_base, k, caen_chan)

                caen_odb_base = "/Equipment/CAEN_HV/Status/Slot %d" % args.caen_slot
                
                for k in self.client.odb_get(caen_odb_base).keys():
                    for idx, caen_chan in enumerate(caen_chan_list):
                        if k.startswith("VMon "):
                            self.odb_measured_path[idx] = "%s/%s[%s]" % (caen_odb_base, k, caen_chan)
                        if k.startswith("IMon "):
                            self.odb_current_path[idx] = "%s/%s[%s]" % (caen_odb_base, k, caen_chan)
        
            self.hv_current_limit = args.caen_current_limit
            self.hv_readback_tolerance = args.caen_readback_tolerance
            self.hv_stability_tolerance = args.caen_stability_tolerance

        self.run_in_parallel = len(self.odb_demand_path) > 1
        self.hv_trip_time = 1000
        self.settling_time_ms = args.settling_time_ms

        if self.run_in_parallel:
            if len(self.cm_pdu_list) != len(self.odb_current_path):
                raise ValueError("PDU list is length %s but channel list is length %s!" % (len(self.cm_pdu_list), len(self.odb_current_path)))
        
            if self.enable_steering:
                raise ValueError("Channel list is only designed to work with control module, not steering module!")

        if args.custom_v:
            regions = args.custom_v.split(",")
            for region in regions:
                (start, stop, step) = [float(x) for x in region.split(":")]
                num_steps = int((stop - start) / step) + 1
                offset = 0
                if len(self.chosen_voltages) and self.chosen_voltages[-1] == start:
                    offset = 1
                self.chosen_voltages.extend([start + step * i for i in range(1,num_steps)])
                print("> Scanning in %.2f V increments between %.2f V and %.2f V" % (step, start, stop))
            print("> Scanning %s points in total" % num_steps)
        else:
            num_steps = int((args.stop_v - args.start_v) / args.step_v) + 1
            self.chosen_voltages = [args.start_v + args.step_v * i for i in range(num_steps)]
            print("> Scanning in %.2f V increments between %.2f V and %.2f V (%s points)" % (args.step_v, args.start_v, args.stop_v, num_steps))
        
        if self.enable_control:
            if self.run_in_parallel:
                print("> Running in parallel for control module PDUs %s" % ", ".join(str(x) for x in self.cm_pdu_list))
            else:
                print("> Stepping through control module PDUs %s" % ", ".join(str(x) for x in self.cm_pdu_list))
            print("> Stepping through control module channels %s" % ", ".join(str(x) for x in self.chan_list))
        elif self.enable_steering:
            print("> Stepping through steering module channels %s" % ", ".join(str(x) for x in self.chan_list))
        else:
            print("> Will not interact with the steering module or control module from this script")

        if args.filename:
            self.file = open(args.filename, "w")
            print("> Writing data to screen and %s" % args.filename)
        else:
            self.file = None
            print("> Writing data to screen only")

        # Turn off all the channels
        if self.enable_steering:
            self.update_sm_settings_and_wait_for_readback()
        if self.enable_control:
            self.update_cm_settings_and_wait_for_readback()

        self.set_hv_trip_settings()
    
    def check_clients_running(self):
        """
        Check that the clients we expect to be running are running.
        This script doesn't work without the Caen driver and steering module
        drivers running.
        """
        others = self.client.odb_get("/System/Clients").values()
        client_caen = False
        client_keithley = False
        client_wiener = False
        client_sm = False
        client_cm = False
        
        for other in others:
            if other["Name"].startswith("CAENHV"):
                client_caen = True

            if other["Name"].startswith("Keithley2450SMU"):
                client_keithley = True

            if other["Name"].startswith("fewiener"):
                client_wiener = True
                
            if other["Name"].startswith(self.sm_client_name):
                client_sm = True
                
            if other["Name"].startswith(self.cm_client_name):
                client_cm = True
                
        if not client_caen and not self.dummy_caen and not self.use_keithley and not self.use_wiener:
            raise RuntimeError("The CAEN frontend is not running")
        
        if self.use_keithley and not client_keithley:
            raise RuntimeError("The Keithley frontend is not running")

        if self.use_wiener and not client_wiener:
            raise RuntimeError("The Wiener frontend is not running")
        
        if self.enable_control and not client_cm:
            raise RuntimeError("The control module frontend %s is not running" % self.cm_client_name)
        
        if self.enable_steering and not client_sm:
            raise RuntimeError("The steering module frontend %s is not running" % self.sm_client_name)

    def update_sm_settings_and_wait_for_readback(self, enable_chan=None):
        """
        Turn on a single steering module channel, and wait for it to be read
        back as on.
        
        Args:
            * enable_chan (int/str/None) -  
                If None, all channels will be off.
                If int, use the old map to turn on channel (0-24) (all others will be off).
                If str, use the PDU+ map to turn on quad.tile ("1.1" to "4.4") (all others will be off).
        """
        if not self.enable_steering:
            return
        
        if self.sm_uses_pduplus:
            # Quad/tile map
            sm_settings = {"Quadrants": [False] * 4}

            for quad in range(1, 5):
                sm_settings["Quadrant %d tiles LV" % quad] = [False] * 4
                sm_settings["Quadrant %d tiles HV" % quad] = [False] * 4

            if enable_chan is not None:
                (our_quad, our_tile) = self.split_quad_tile(enable_chan)
                sm_settings["Quadrants"][our_quad - 1] = True
                sm_settings["Quadrant %d tiles LV" % our_quad][our_tile - 1] = True
                sm_settings["Quadrant %d tiles HV" % our_quad][our_tile - 1] = True

            #print("Setting %s/Settings/PDUPlus map to %s" % (self.sm_equip_base, sm_settings))
            self.client.odb_set("%s/Settings/PDUPlus map" % self.sm_equip_base, sm_settings, remove_unspecified_keys=False)
            
            timeout = datetime.datetime.now() + datetime.timedelta(seconds=5)

            while True:
                rdb = self.client.odb_get("%s/Readback/PDUPlus map" % self.sm_equip_base)
                all_correct = True

                for section in sm_settings.keys():
                    if rdb[section] != sm_settings[section]:
                        all_correct = False
                    
                if all_correct:
                    break
                
                if datetime.datetime.now() > timeout:
                    if enable_chan is None:
                        raise RuntimeError("Steering module didn't respond to request to disable all channels; readback is %s" % (rdb))
                    else:
                        raise RuntimeError("Steering module didn't respond to request to enable channel %s; readback is %s" % (enable_chan, rdb))

                self.client.communicate(10)
        else:
            # Old-style map
            sm_settings = {"High voltage": [False] * 25, "Low voltage": [False] * 25}
            
            if enable_chan is not None:
                sm_settings["High voltage"][enable_chan] = True
                sm_settings["Low voltage"][enable_chan] = True

            # HV/LV is in "Settings/"" for old installs, "Settings/Original map" for newer ones
            set_path = "%s/Settings/Original map" % self.sm_equip_base
            rdb_path = "%s/Readback/Original map" % self.sm_equip_base

            if not self.client.odb_exists(set_path):
                set_path = "%s/Settings" % self.sm_equip_base
                rdb_path = "%s/Settings" % self.sm_equip_base

            self.client.odb_set(set_path, sm_settings, remove_unspecified_keys=False)
            
            timeout = datetime.datetime.now() + datetime.timedelta(seconds=5)

            while True:
                rdb = self.client.odb_get(rdb_path)
                
                if rdb["High voltage"] == sm_settings["High voltage"] and rdb["Low voltage"] == sm_settings["Low voltage"]:
                    break
                
                if datetime.datetime.now() > timeout:
                    if enable_chan is None:
                        raise RuntimeError("Steering module didn't respond to request to disable all channels; readback is %s" % (rdb))
                    else:
                        raise RuntimeError("Steering module didn't respond to request to enable channel %s; readback is %s" % (enable_chan, rdb))

                self.client.communicate(10)
    
    def update_cm_settings_and_wait_for_readback(self, enable_pdu=None, enable_chan=None):
        """
        Turn on a single control module channel, and wait for it to be read
        back as on.
        
        Args:
            * enable_pdu (int/list of int/None) -
                If None, all 16 PDUs will be affected
                If int, only that PDU will be affected
                If list of int, only this PDUs will be affected
            * enable_chan (int/str/None) -  
                If None, all channels will be off.
                If int, use the old map to turn on channel (0-24) (all others will be off).
                If str, use the PDU+ map to turn on quad.tile ("1.1" to "4.4") (all others will be off).
        """
        if not self.enable_control:
            return
        
        if isinstance(enable_pdu, list):
            pdu_list = enable_pdu
        elif enable_pdu is None:
            pdu_list = self.cm_pdu_list
        else:
            pdu_list = [enable_pdu]

        cm_settings = {}

        for pdu in pdu_list:
            cm_settings[pdu] = {"Quadrants": [False] * 4}

            for quad in range(1, 5):
                cm_settings[pdu]["Quadrant %d tiles LV" % quad] = [False] * 4
                cm_settings[pdu]["Quadrant %d tiles HV" % quad] = [False] * 4

            if enable_chan is not None:
                (our_quad, our_tile) = self.split_quad_tile(enable_chan)
                cm_settings[pdu]["Quadrants"][our_quad - 1] = True
                cm_settings[pdu]["Quadrant %d tiles LV" % our_quad][our_tile - 1] = True
                cm_settings[pdu]["Quadrant %d tiles HV" % our_quad][our_tile - 1] = True

            self.client.odb_set("%s/Settings/PDU %d" % (self.cm_equip_base, pdu), cm_settings[pdu], remove_unspecified_keys=False)
            
        timeout = datetime.datetime.now() + datetime.timedelta(seconds=15)
                
        while True:
            all_correct = True

            for pdu in pdu_list:
                rdb = self.client.odb_get("%s/Readback/PDU %d" % (self.cm_equip_base, pdu))

                for section in cm_settings[pdu].keys():
                    if rdb[section] != cm_settings[pdu][section]:
                        all_correct = False
                
                if datetime.datetime.now() > timeout:
                    if enable_chan is None:
                        raise RuntimeError("Control module didn't respond to request to disable all channels on PDU %d; readback is %s" % (pdu, rdb))
                    else:
                        raise RuntimeError("Control module didn't respond to request to enable channel %s on PDU %d; readback is %s" % (enable_chan, pdu, rdb))

            if all_correct:
                break
                
            self.client.communicate(10)

    def set_hv_trip_settings(self):
        """
        Set the safety parameters of the Caen HV (trip time and current limit)
        """
        if self.dummy_caen:
            return

        for path in self.odb_trip_path:
            if path is not None:
                self.client.odb_set(path, self.hv_trip_time)
            
        for path in self.odb_limit_path:
            if path is not None:
                self.client.odb_set(path, self.hv_current_limit)
    
    def set_hv_in_odb(self, voltage, idx=0):
        if self.dummy_caen:
            self.dummy_caen_v = voltage
            self.dummy_caen_fake_failure[idx] = random.uniform(0, 1) < 0.03
            return

        self.client.odb_set(self.odb_demand_path[idx], voltage)

    def get_hv_from_odb(self, idx=0):
        if self.dummy_caen:
            if idx in self.dummy_caen_fake_failure and self.dummy_caen_fake_failure[idx]:
                return self.dummy_caen_v - self.hv_readback_tolerance * 2
            
            return self.dummy_caen_v

        return self.client.odb_get(self.odb_measured_path[idx])  
    
    def turn_hv_on_if_needed(self, idxs=None):
        """
        This should be called at the start of each voltage scan if there's
        a possibility that the channel will be turned off automatically
        (e.g. Wiener turns channel off if voltage is set to 0V).

        Args:
            * idxs (list of int/None)
        """
        if idxs is None:
            idxs = list(range(len(self.cm_pdu_list))) if self.run_in_parallel else [0]

        for idx in idxs:
            if self.odb_switch_path[idx] is not None:
                self.client.odb_set(self.odb_switch_path[idx], True)  

    def set_hv_and_wait_for_readback(self, voltage, idxs=None, is_first_v=False):
        """
        Set the HV to the specified voltage, and wait for the readback to
        match what is set. We'll calculate how long to wait based on the ramp
        up/down speeds (if known); if it takes too long after that, we'll give up.
        
        Args:
            * voltage (float)
            * idxs (list of int/None)
            * is_first_v (bool) - whether this is the start of a new scan
        """
        if idxs is None:
            idxs = list(range(len(self.cm_pdu_list))) if self.run_in_parallel else [0]

        max_expected_ramp_secs = 0
        self.dummy_caen_fake_failure = {}

        for idx in idxs:
            prev_voltage = self.get_hv_from_odb(idx)
            delta_voltage = voltage - prev_voltage
            
            if delta_voltage > 0:
                if self.odb_ramp_up_path[idx] is None:
                    expected_ramp_secs = 1
                else:
                    expected_ramp_secs = delta_voltage / self.client.odb_get(self.odb_ramp_up_path[idx])
            else:
                if self.odb_ramp_down_path[idx] is None:
                    expected_ramp_secs = 1
                else:
                    expected_ramp_secs = -1 * delta_voltage / self.client.odb_get(self.odb_ramp_down_path[idx])
            
            max_expected_ramp_secs = max(max_expected_ramp_secs, expected_ramp_secs)

            self.set_hv_in_odb(voltage, idx)

            if is_first_v:
                # Turn on after setting new voltage
                self.turn_hv_on_if_needed(idxs)

        timeout = datetime.datetime.now() + datetime.timedelta(seconds=max_expected_ramp_secs + 10)

        count = 0
        idxs_to_do = {i: True for i in idxs}

        while True:
            if count == 5: #set voltage commands don't always work
                raise RuntimeError("HV didn't respond to request to set voltage to %s; latest readback voltage is %s " % (voltage, rdb))

            for idx in [i for i,to_do in idxs_to_do.items() if to_do]:
                rdb = self.get_hv_from_odb(idx)
                
                if abs(rdb - voltage) < self.hv_readback_tolerance:
                    idxs_to_do[idx] = False
                
                if datetime.datetime.now() > timeout:
                    print("HV didn't respond to request to set voltage at %s to %s. Trying again." % (self.odb_demand_path[idx], voltage))
                    time.sleep(1)
                    self.set_hv_in_odb(voltage, idx)
                    count += 1
                    timeout = datetime.datetime.now() + datetime.timedelta(seconds=max_expected_ramp_secs + 10)
            
            if len([i for i,to_do in idxs_to_do.items() if to_do]) == 0:
                # All done
                break

            self.client.communicate(10)
    
    def get_hv_stable_current(self, idxs=None):
        """
        Get the HV current. We'll wait until two successive readings (0.5s
        apart) are within a set tolerance. If we haven't reached stability 
        within a reasonable time, we'll give up.
        """
        if idxs is None:
            idxs = list(range(len(self.cm_pdu_list))) if self.run_in_parallel else [0]

        timeout = datetime.datetime.now() + datetime.timedelta(seconds=5)
        prev_read = -100000

        retval = {idx:None for idx in idxs}

        if self.dummy_caen:
            for idx in retval.keys():
                retval[idx] = (self.dummy_caen_v * 10) + (idx * 0.1)
            return retval
        
        while True:
            for idx in retval.keys():
                if retval[idx] is not None:
                    continue

                this_read = self.client.odb_get(self.odb_current_path[idx])
                
                if abs(prev_read - this_read) < self.hv_stability_tolerance:
                    retval[idx] = this_read
                
                if datetime.datetime.now() > timeout:
                    raise RuntimeError("HV hasn't reached a stable current; latest readback current is %s" % (this_read))
                
                prev_read = this_read

            if len([i for i,curr in retval.items() if curr is None]) == 0:
                # All read
                break

            self.client.communicate(500)

        return retval
    
    def split_quad_tile(self, quad_tile):
        """
        Split a string of the form "3.4" into quadrant and tile number (3, 4).
        """
        (quad, tile) = [int(x) for x in quad_tile.split(".")]
        return (quad, tile)

    def print_line(self, line):
        """
        Print to screen, and optionally to file.
        
        Args:
            * line (str)
        """
        print(line)
        if self.file:
            self.file.write(line + "\n")
    
    def run(self):
        """
        Main function to run the scan.
        """
        if self.enable_control:
            self.print_line("PDU,Quadrant,Tile,Voltage (V),Current (uA)")
        elif self.enable_steering:
            if self.sm_uses_pduplus:
                self.print_line("Quadrant,Tile,Voltage (V),Current (uA)")
            else:
                self.print_line("Channel,Voltage (V),Current (uA)")
        else:
            self.print_line("Voltage (V),Current (uA)")
        
        if self.run_in_parallel:
            self.run_parallel()
        else:
            self.run_single()

    def run_parallel(self):

        for chan in self.chan_list:
            (quad,tile) = self.split_quad_tile(chan)
            self.update_cm_settings_and_wait_for_readback(self.cm_pdu_list, chan)
            idxs_to_control = {idx: True for idx in range(len(self.cm_pdu_list))}

            for vidx, voltage in enumerate(self.chosen_voltages):
                idx_list_this_v = [i for i,to_do in idxs_to_control.items() if to_do]
                self.set_hv_and_wait_for_readback(voltage, idx_list_this_v, is_first_v=(vidx==0))
                self.client.communicate(self.settling_time_ms)
                currents = self.get_hv_stable_current(idx_list_this_v)
                
                for idx, pdu in enumerate(self.cm_pdu_list):
                    if idx not in idx_list_this_v:
                        continue

                    self.print_line("%s,%s,%s,%s,%s" % (pdu, quad,tile,voltage,currents[idx]))
                
                    if currents[idx] > self.hv_current_limit * 0.95:
                        # Reached the current limit. Either abort or move to the
                        # next channel.
                        if self.abort_on_high_hv_current:
                            self.set_hv_in_odb(0, idx)
                            raise ValueError("Current got too close to limit - ramping down to 0V")
                        else:
                            idxs_to_control[idx] = False

                under_limit = [i for i,to_do in idxs_to_control.items() if to_do]

                if len(under_limit) == 0:
                    break

            self.set_hv_and_wait_for_readback(0.0)

        # Turn off all the channels at the end.
        self.update_cm_settings_and_wait_for_readback(self.cm_pdu_list, None)

    def run_single(self):
        pdu_list = self.cm_pdu_list if self.enable_control else [None]

        for pdu in pdu_list:
            for chan in self.chan_list:
                if self.enable_steering:
                    self.update_sm_settings_and_wait_for_readback(chan)

                if self.enable_control:
                    self.update_cm_settings_and_wait_for_readback(pdu, chan)

                for vidx, voltage in enumerate(self.chosen_voltages):
                    self.set_hv_and_wait_for_readback(voltage, is_first_v=(vidx==0))
                    self.client.communicate(self.settling_time_ms)
                    current = self.get_hv_stable_current()[0]
                    
                    if self.enable_control:
                        (quad,tile) = self.split_quad_tile(chan)
                        self.print_line("%s,%s,%s,%s,%s" % (pdu, quad,tile,voltage,current))
                    elif self.enable_steering:
                        if self.sm_uses_pduplus:
                            (quad,tile) = self.split_quad_tile(chan)
                            self.print_line("%s,%s,%s,%s" % (quad,tile,voltage,current))
                        else:
                            self.print_line("%s,%s,%s" % (chan,voltage,current))
                    else:
                        self.print_line("%s,%s" % (voltage,current))

                    if current > self.hv_current_limit * 0.95:
                        # Reached the current limit. Either abort or move to the
                        # next channel.
                        if self.abort_on_high_hv_current:
                            self.set_hv_in_odb(0)
                            raise ValueError("Current got too close to limit - ramping down to 0V")
                        else:
                            break

                self.set_hv_and_wait_for_readback(0.0)

            # Turn off all the channels at the end.
            if self.enable_steering:
                self.update_sm_settings_and_wait_for_readback(None)
            if self.enable_control:
                self.update_cm_settings_and_wait_for_readback(pdu, None)
                
if __name__ == "__main__":
    iv = IVCurve()
    iv.parse_args()
    iv.check_clients_running()
    iv.run()
